<?php
  header("Location: /");
  exit;

  require_once "config.inc.php";
  require_once "lib/errorcache.inc.php";
  require_once "lib/database.inc.php";

  define('MAGPIE_DIR', '');
  require_once "extlib/rss_parse.inc";
  require_once "extlib/Snoopy.class.inc";

  // Open connection to database.
  $DB = new Database();
  if( ! $DB->openConnection($DATABASE_URL) )
  {
    print_error_report();
    return;
  }

  startUpdate();

  $DB->closeConnection();
  exit;


  /**
   * Update the feeds in the database.
   */
  function startUpdate()
  {
    global $DB;

    // Get feeds
    $sql = 'select * from rss_feeds where active';
    if( ( $feeds = $DB->select($sql) ) === false )
    {
      print_error_report();
      return;
    }

    if( $feeds->getRowCount() == 0 )
    {
      print "no active feeds.";
    }

    // Download feeds, based on magpierss/rss_fetch.php.
    // re-implemented here to replace the magpie cache with MySQL,
    // and avoid calling private functions of rss_fetch.php
    while( ( $feed = $feeds->getRecord() ) )
    {
      $http =& downloadFeed( $feed );

      if( $http === false )
      {
        // error.
        print_error_report();
      }
      else if( $http === true )
      {
        // not modified.
        print "Feed '" . htmlentities($feed['url']) . "' is not modfied.<br>\n";
      }
      else
      {
        parseRss( $http, $feed );
      }
    }
    print "end update";
  }


  function &downloadFeed( &$feed )
  {
    // When last modified date is know, use it to request all items from that date.
    // Should work with most standards based blog software.
    $customHeaders = array();
    if( strlen($feed['etag']) > 0 )
    {
       $customHeaders['If-None-Match']    = $feed['etag'];
       $customHeaders['If-Last-Modified'] = $feed['last_modified'];
    }

    // Download feed contents
    $http =& createHttpClient( $customHeaders );  // $http is the Snoopy HTTP user agent.
    $http->fetch( $feed['url'] );

    // Process the HTTP status/response.
    $statusCode = $http->status;
    if( 200 <= $statusCode && $statusCode <= 299 )
    {
      // 200 OK, HTTP succes range.
      return $http;
    }
    if( $statusCode == '304' )
    {
      // 304 Not Modified response.
      $status = true;
      return $status;
    }
    else
    {
      // HTTP error or redirect.
      $errorMsg = "Failed to download RSS file " . htmlentities( $feed['url'] );
      if( $http->status == '-100' )
      {
        $errorMsg .= "(Request timed out)";
      }
      elseif( $http->error )
      {
        $errorMsg .= '(HTTP Error: ' . rtrim( $http->error ) . ')';  // remove added \n
      }
      else
      {
        $errorMsg .= "(HTTP Response: " . $http->response_code . ')';
      }

      ErrorCache::setError( $errorMsg );
      return false;
    }
  }


  /**
   * Fetch a remote HTTP file.
   * @returns Snoopy  The Snoopy class to read the data from.
   */
  function createHttpClient( $headers = array() )
  {
    // Snoopy is an HTTP client in PHP
    // Code based on magpierss/rss_fetch.php
    $client = new Snoopy();
    $client->agent        = "PHPPlanet/0.1";
    $client->read_timeout = 60;
    $client->use_gzip     = true;
    if( ! empty($headers) )
    {
      $client->rawheaders = $headers;
    }

    return $client;
  }


  /**
   * Process a downloaded feed.
   */
  function parseRss( &$http, &$feed )
  {
    // MagpieRSS( data, outputEncoding, inputEncoding, detectEncoding );
    $rss = new MagpieRSS( $http->results, "utf-8", null, true );

    // if RSS parsed successfully
    if( ! $rss || $rss->ERROR )
    {
      $errormsg = "Failed to parse RSS file.";
      if($rss)
      {
        $errormsg .= " (" . $rss->ERROR . ")";
      }

      print $errormsg;
      return false;
    }

    // find Etag, and Last-Modified in HTTP headers.
    // Update $feed with the new data.
    foreach( $http->headers as $headerLine )
    {
      if( strpos($headerLine, ": ") !== false )
      {
        list($field, $value) = explode(": ", $headerLine, 2);
      }
      else
      {
        $field = $headerLine;
        $value = "";
      }

      if( $field == 'ETag' )
      {
        $feed['etag'] = $value;
      }
      else if( $field == 'Last-Modified' )
      {
        $feed['last_modified'] = $value;
      }
    }


    // Process the RSS items.
    return saveRss( $rss, $feed );
  }


  /**
   * Save the RSS and modified feed record.
   */
  function saveRss( &$rss, &$feed )
  {
    global $DB;

    $prevTime1 = 0;
    $prevTime2 = 0;

    // Add field items to database.
    foreach( $rss->items as $item )
    {
      // Build record to insert
      $itemTime = 0;
      $record = array();
      $record['feed_id']  = $feed['id'];
      $record['title']    = $item['title'];
      $record['url']      = isset($item['about']) ? $item['about'] : $item['link'];
      $record['guid']       = null;
      $unescapedFields = array( 'pubdate' );

      // Get summary and contents
      if( isset($item['summary']) )
      {
        $record['summary'] = $item['summary'];
      }

      if( isset($item['content']) )
      {
        if( is_array($item['content']) )
        {
          if( isset($item['content']['encoded']) )
          {
            $record['contents'] = html_entity_decode($item['content']['encoded']);
          }
        }
        else
        {
          $record['contents'] = $item['content'];
        }
      }
      else if( isset($item['description']) )
      {
        $record['contents'] = $item['description'];
      }

      if( isset($record['contents']) )
      {
        $record['md5sum'] = md5($record['contents']);
      }

      // guid can be optional too.
      if( isset($record['guid']) )
      {
        $record['guid'] = $item['guid'];
      }

      // Extract the time
      if( isset($item['date_timestamp']) )
      {
        $itemTime = $item['date_timestamp'];
      }
      else if( isset($item['dc']['date']) )
      {
        $itemTime = parse_w3cdtf($item['dc']['date']);
      }

      // Check whether the date differs, or all blog articles have to same date (flooding the planet).
      // This happens you update a template in blogger.com for instance.
      if( $prevTime2 != 0 )
      {
        if( $itemTime == $prevTime1 && $itemTime == $prevTime2 )
        {
          print "skipping '" . $record['title'] . "', feed pubdate seams to be broken<br/>\n";
          continue;
        }
      }
      $prevTime2 = $prevTime1;
      $prevTime1 = $itemTime;

      // Get the date fields.
      if( $itemTime != 0 )
      {
        $record['pubdate']  = 'FROM_UNIXTIME(' . (double) $itemTime . ')';
      }

      // Get author from RSS or DC fields.
      if( isset($item['author']) )
      {
        $record['author'] = $item['author'];
      }
      else if( isset($item['dc']['creator']) )
      {
        $record['author'] = $item['dc']['creator'];
      }

      // Get category from RSS or DC fields.
      if( isset($item['category']) )
      {
        $record['category'] = $item['category'];
      }
      else if( isset($item['dc']['subject']) )
      {
        $record['category'] = $item['dc']['subject']; // Dublin Core meta data
      }

      // Find out whether the item was already added.
      $dupWhere = "feed_id=? and (guid=? or url=?)";
      $dupArgs  = array( $feed['id'], $record['guid'], $record['url'] );
      $sql = "select * from rss_items where " . $dupWhere;
      if( ( $res = $DB->select($sql, $dupArgs) ) === false )
      {
        print_error_report();
        return false;
      }
      $count = $res->getRowCount();

      if( $count > 0 )
      {
        // Already added.
        if( $count > 1 )
        {
          print "skipping '" . $record['title'] . "', too many duplicates found<br/>\n";
        }
        else
        {
          // One record found, update it
          print "updating '" . $record['title'] . "'<br/>\n";
          if( $DB->update( 'rss_items', $record, $dupWhere, $dupArgs, $unescapedFields ) === false )
          {
            print_error_report();
            return false;
          }
        }
      }
      else
      {
        // Insert item in database/
        print "inserting '" . $record['title'] . "'<br/>\n";
        $record['date_added'] = 'NOW()';
        $unescapedFields[] = 'date_added';
        if( $DB->insert( 'rss_items', $record, $unescapedFields ) === false )
        {
          print_error_report();
          return false;
        }
      }
    }

    // Update feed in database.
    $feed['date_updated'] = 'NOW()';
    $unescapedFields = array( 'date_updated' );
    if( $DB->update( 'rss_feeds', $feed, "id=?", array($feed['id']), $unescapedFields ) === false )
    {
      print_error_report();
      return false;
    }
  }


?>
