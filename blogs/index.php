<?php
  header("Location: /");
  exit;

  require_once "config.inc.php";
  require_once "lib/errorcache.inc.php";
  require_once "lib/database.inc.php";
  require_once "lib/template.inc.php";

  // Open connection to database.
  $DB = new Database();
  if( ! $DB->openConnection($DATABASE_URL) )
  {
    print_error_report();
    return;
  }

  // Determine template parameters.
  $encoding = 'html';
  $templateFile = 'mainpage.html';
  if( isset($_REQUEST['rss']) )
  {
    $encoding = 'xml';
    $templateFile = 'mainpage.rss';
  }

  // Load template.
  $tpl = new Template();
  if( ! $tpl->loadFile("templates/" . $templateFile) )
  {
    print_error_report();
    exit;
  }
  $tpl->setDefaultEncoding( $encoding );

  // Process the RSS items.
  if( processRssItems( $tpl, 40 ) === false )
  {
    print_error_report();
    return;
  }

  // Fill the template with site meta data
  $fields = array( 'title', 'rss_title', 'description', 'language', 'url' );
  foreach( $fields as $fieldName )
  {
    $value = isset( $SITE_META_DATA[$fieldName] ) ? $SITE_META_DATA[$fieldName] : "";
    $tpl->replace( "site:" . $fieldName, $value );
  }

  // Print template
  print $tpl->getResult();
  return;



  /**
   * Process the RSS items in the template.
   */
  function processRssItems( &$tpl, $itemCount = 40 )
  {
    global $DB;

    // Get feed titles.
    $feedData = getFeedData();
    if( $feedData === false )
    {
      return false;
    }

    // Get feed items.
    $sql = "select * from rss_items order by pubdate desc limit " . (int) $itemCount;
    if( ( $res = $DB->select($sql) ) === false )
    {
      return false;
    }

    // process in template.
    $tpl->set( 'results', ! $res->isEmpty() );
    $block = $tpl->getBlock("rssitems");
    if( $block !== false )
    {
      $prevDay    = '';
      $prevFeed   = '';
      $prevAuthor = '';
      while( $record = $res->getRecord() )
      {
        $block->appendSource();

        // Get values
        $pubtime  = strtotime($record['pubdate']);
        $rssDate  = strftime("%a, %d %b %Y %H:%M:%S %z", $pubtime);
        $day      = strftime("%B %d, %Y", $pubtime);
        $fulldate = strftime("%B %d, %Y %I:%M %p", $pubtime);
        $author   = $record['author'];
        $feedId   = $record['feed_id'];
        $feed     = $feedData[ $feedId ];

        // Handle sets
        $newFeed   = ( $feedId != $prevFeed   );
        $newDay    = ( $day    != $prevDay    );
        $newAuthor = ( $author != $prevAuthor );
        $block->set( 'newday', $newDay );
        $block->set( 'newfeed', $newFeed || $newDay );
        $block->set( 'newauthor', $newAuthor || $newFeed || $newDay );

        // Replace additional values.
        $block->replace( 'day', $day );
        $block->replace( 'author', $author );
        $block->replace( 'feed_title', $feed['title'] );
        $block->replace( 'feed_siteurl', $feed['siteurl'] );
        $block->replace( 'fulldate', $fulldate );
        $block->replace( 'pubdate_rfc', $rssDate );

        // Fix URL's in summary and contents.
        $summary  = fixRelativeUrls( $record['summary'],  $record['url'] );
        $contents = fixRelativeUrls( $record['contents'], $record['url'] );

        // Replace contents before the rest so it's not HTML escaped.
        $block->replaceTag( 'raw_summary', $summary );
        $block->replaceTag( 'raw_contents', $contents );

        // Replace modified summary and contents
        $block->replace( 'summary', $summary );
        $block->replace( 'contents', $contents );

        // Default to URL when GUID is empty (for atom feeds).
        if( $record['guid'] == '' )
        {
          $record['guid'] = $record['url'];
        }

        // Replace all remaining tags
        foreach( $record as $key => $value )
        {
          $block->replace( $key, $value );
        }

        // Update state
        $prevFeed   = $feedId;
        $prevDay    = $day;
        $prevAuthor = $author;
      }
      $tpl->replaceBlock( $block );
    }

    return true;
  }


  /**
   * Return the feed information
   */
  function getFeedData()
  {
    global $DB;

    // Run SQL
    $sql = "select id, title, siteurl from rss_feeds";
    if( ( $res = $DB->select($sql) ) === false )
    {
      return false;
    }

    // Convert to array
    $feedTitles = array();
    while( $record = $res->getRecord() )
    {
      $feedTitles[ $record['id'] ] = $record;
    }

    return $feedTitles;
  }


  /**
   * Fix the relative URL's in feed contents.
   */
  function fixRelativeUrls( $string, $baseUrl )
  {
    // Avoid testing for invalid URL's.
    if( $baseUrl == '' || strpos($baseUrl, "://") === false )
    {
      return $string;
    }

    // Remove query string.
    if( ( $pos = strpos($string, "?") ) !== false )
    {
      $baseUrl = substr($baseUrl, 0, $pos);
    }

    // Explode to http://.../..
    $urlParts = explode( "/", $baseUrl, 4);
    $baseSite = $urlParts[0] . '//' . $urlParts[2];

    // When there is a filename at the end, remove it.
    $baseDir = $baseUrl;
    if( $urlParts[ count($urlParts) - 1 ] !== "" )
    {
      $baseDir = rtrim(dirname($baseSite), '/') . '/';
    }

    // Find URL's in the string
    $pos = 0;
    $i = 0;
    while( true )
    {
      if( $i++ > 4 ) break;
      $cap = array();
      if( preg_match( '/ (href|src)=["\']([^(http|mailto)][^"\']+)["\']/', $string, $cap, PREG_OFFSET_CAPTURE, $pos ) === false
      ||  empty( $cap ) )
      {
        break;
      }

      if( ! defined( $cap[3] ) )
      {
      print_r( $cap );
      }

      $pos = $cap[0][1];
      $url = $cap[3][0];

      if( $url{0} == '/' )
      {
        $url = $baseSite . $url;  // append site name.
      }
      else
      {
        $url = $baseDir . $url;  // Let the browser parse out the "../" parts in the URL.
      }

      // Concat: "before (img|src)=["']$url["']after"
      $replacement = ' ' . $cap[1][0] . '=' . $cap[2][0] . $url . $cap[3][0];
      $string = substr( $string, 0, $pos ) . $replacement  . substr( $string, $pos + strlen($cap[0][0]) );
      $pos += strlen( $replacement );
    }

    return $string;
  }

?>
<h1>The developer blogs are not available any more, as this is an archived project. Sorry!</h1>
