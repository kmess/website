<?php

require_once "lib/errorcache.inc.php";



/**
 * A class maintaining the connection to the database.
 */
class Database
{
  // Variables storing the database information:
  var $dbh = null;
  var $lastStatement = null;


  // ------------------------------
  // Class Functions

  /**
   * The constructor connects to the database.
   */
  function Database()
  {
  }


  // ------------------------------
  // Connection handling

  /**
   * Connect to the databasae.
   * The 'dns' should be something like mysql://user:pass@host/database
   */
  function openConnection( $dsn )
  {
    // Parse DSN to array fields
    if( ! is_array($dsn) )
    {
      $dsn = parse_url($dsn);
    }

    // Connect
    $this->dbh = @mysql_connect( $dsn['host'], $dsn['user'], $dsn['pass'] );
    if( $this->dbh === false )
    {
      $this->dbh = null;
      ErrorCache::setError( "Unable to connect to the database.\n" . mysql_error() );
      return false;
    }

    // Select database
    if( mysql_select_db( trim($dsn['path'], '/'), $this->dbh ) === false )
    {
      mysql_close($this->dbh);
      $this->dbh = null;
      ErrorCache::setError( "Unable to connect to the database.\n" . mysql_error() );
      return false;
    }

    return true;
  }

  
  /**
   * Close the connection to the database.
   */
  function closeConnection()
  {
    if( $this->dbh != null )
    {
      mysql_close($this->dbh);
      $this->dbh = null;
    }
  }


  // ------------------------------
  // SQL Functions


  /**
   * Request one record from a table/query.
   * Returns the array when successful, false otherwise.
   */
  function select( $sql, $args = array() )
  {
    // Execute SQL
    if( ( $res =& $this->_executeSql($sql, $args) ) === false )
    {
      return false;
    }

    // Build resultset
    return new ResultSet( $res );
  }

    
  /**
   * Runs an SQL command, returns the number of affected rows.
   */
  function &execute( $sql, $args = array() )
  {
    return $this->_executeSql($sql, $args);
  }


  /**
   * Insert data in a table, returns the last inserted ID.
   * @param  string  The database table.
   * @param  array   An associative array with fields/values.
   * @param  array   The fields that should not be escaped, to use functions. Handle with care!
   */
  function insert( $table, $fields, $unescapedFields = array() )
  {
    // Build insert into statement.
    $sql = 'insert into `' . $table . '` (';
    $sqlValues = ') values (';
    $args = array();
    $i = 0;
    foreach( $fields as $fieldName => $value )
    {
      // Add seperators
      if( $i > 0 )
      {
        $sql       .= ', ';
        $sqlValues .= ', ';
      }
      $i++;

      // Add fieldname
      $sql .= '`' . $fieldName . '`';

      // Add value
      if( in_array($fieldName, $unescapedFields) )
      {
        $sqlValues .= $value;
      }
      else
      {
        $sqlValues .= "?";
        $args[] = $value;
      }
    }
    $sql .= $sqlValues . ')';

    // Execute SQL
    if( $this->_executeSql($sql, $args) === false )
    {
      return false;
    }

    return mysql_insert_id( $this->dbh );
  }


  /**
   * Insert data in a table, return success or failure.
   * @param  string  The database table.
   * @param  array   An associative array with fields/values.
   * @param  string  The where condition for the update.
   * @param  array   The placeholders in the where statement.
   * @param  array   The fields that should not be escaped, to use functions. Handle with care!
   */
  function update( $table, $fields, $where, $whereArgs = array(), $unescapedFields = array() )
  {
    $sql = 'update `' . $table . '` set ';
    $args = array();
    $i = 0;
    foreach( $fields as $fieldName => $value )
    {
      // Add seperators
      if( $i > 0 )
      {
        $sql .= ', ';
      }
      $i++;

      // Add fieldname
      $sql .= '`' . $fieldName . '`=';

      // Add value
      if( in_array($fieldName, $unescapedFields) )
      {
        $sql .= $value;
      }
      else
      {
        $sql .= "?";
        $args[] = $value;
      }
    }

    // Add where
    $sql .= ' where ' . $where;
    $args = array_merge( $args, $whereArgs );

    // Run query
    if( ( $res =& $this->_executeSql($sql, $args) ) === false )
    {
      return false;
    }

    return true;
  }


  /**
   * Escape a value for safe inclusion in a string.
   */
  function escape( $value )
  {
    if( is_null($value) )
    {
      return 'null';
    }
    else if( is_numeric($value) )
    {
      return $value;
    }
    else
    {
      return "'" . mysql_real_escape_string( $value, $this->dbh ) . "'";
    }
  }


  /**
   * Return the number of records.
   */
  function getRecordCount( $table, $where = '1', $whereArgs = array() )
  {
    // Build SQL.
    $sql = 'select count(*) as total from `' . $table . '`';
    if( strlen($where) > 0 && $where !== '1' )
    {
      $sql .= ' where ' . $where;
    }

    // Run query
    if( ( $res =& $this->_executeSql($sql, $whereArgs) ) === false )
    {
      return false;
    }

    // Return rowcount.
    $record = $res->getRecord();
    return $record['total'];
  }


  /**
   * Return the last executed statement.
   */
  function getLastStatement()
  {
    return $this->lastStatement;
  }


  /**
   * Execute an SQL statement.
   */
  function &_executeSql( $sql, $args )
  {
    // Replace placeholders with actual values.
    $pos = 0;
    $error = false;
    foreach( $args as $value )
    {
      $pos = strpos( $sql, "?", $pos );
      if( $pos === false )
      {
        ErrorCache::setError( "Unable to prepare query, argument value list is too long!" );
        return $error;
      }

      // Escape value.
      $value = $this->escape($value);

      // Check for missing quotes.
      // They could be added here, but they can't for $this->escape()
      // Instead of fixing things automatically make it mandatory to write safe queries.
      if( $pos != 0 && ( $sql{$pos - 1} == "'" || $sql{$pos - 1} == '"' ) )
      {
        ErrorCache::setError( "Unable to prepare query '" . $sql . ".\n"
                            . "Query already adds quotes arround placeholder." );
        return $error;
      }
      if( $pos != (strlen($sql) - 1) && ( $sql{$pos + 1} == "'" && $sql{$pos + 1} == '"' ) )
      {
        ErrorCache::setError( "Unable to prepare query '" . $sql . ".\n"
                            . "Query already adds quotes after placeholder." );
        return $error;
      }

      $sql = substr($sql, 0, $pos) . $value . substr($sql, $pos + 1);
      $pos += strlen($value);
    }

    // Check if there are still placeholders left
    if( $pos !== false )
    {
      $pos = strpos( $sql, "?", $pos );
      if( $pos !== false )
      {
        ErrorCache::setError( "Unable to prepare query, argument value list is too short!" );
        return $error;
      }
    }

    // Run query
    $this->lastStatement = $sql;
    $res = mysql_query( $sql, $this->dbh );
    if( $res === false )
    {
      ErrorCache::setError( "Unable to execute query\n" . mysql_error() );
      return $error;
    }


    return $res;
  }
}



/**
 * A resultset wrapper object.
 */
class ResultSet
{
  var $data = null;
  var $rows = 0;

  
  /**
   * Constructor
   */
  function ResultSet( &$data )
  {
    $this->data =& $data;
    $this->rows = mysql_num_rows( $this->data );
  }


  /**
   * Return a record from the resultset.
   */
  function &getRecord()
  {
    // Get row
    $record = mysql_fetch_assoc( $this->data );
    if( $record === false )
    {
      return $record;  // last row.
    }

    return $record;
  }


  /**
   * Return the number of rows.
   */
  function getRowCount()
  {
    // rowCount() returns the number of rows fetched.
    return $this->rows;
  }


  /**
   * Return whether the resultset has no records.
   */
  function isEmpty()
  {
    return $this->rows == 0;
  }


  /**
   * Free the record set.
   */
  function free()
  {
    mysql_free_result( $this->data );
    $this->data = null;
    $this->rows = 0;
  }
}



?>
