<?php

/**
    Simple Template engine class.
    Copyright (C) 2006  Diederik van der Boor
    Blocks/tags/sets design inspired by V.H.W.Duthler

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once "errorcache.inc.php";


/**
 * Simple straight forward template engine.
 * Uses string-replacement functions.
 *
 * Example template file:
 * >  <p>Hallo <!--[name]--></p>
 * >  <ul>
 * >  <!--[[begin friends]]-->
 * >    <li><!--[name]--></li>
 * >  <!--[[end friends]]-->
 * >  </ul>
 *
 * Templates can be loaded with:
 * >  $tpl = new Template();
 * >  $tpl->loadFile( "test.html" ) or die("..");
 *
 * Tags can be replaced with:
 * >  $tpl->replace( "name", $userName );
 *
 * Repeated blocks can be created with:
 * >  $block = $template->getBlock("friends");
 * >  if( $block !== false ) {
 * >    foreach( $friends as $item ) {
 * >      $block->appendSource();
 * >      $block->replace( 'name', $friend['name'] );
 * >    }
 * >    $tpl->replaceBlock($block);  // writes result back to parent.
 * >  }
 *
 * The template engine does not try to "smart parse" or "execute code" in templates.
 * The template file only marks/tags the locations, which can be put to use from PHP processing code.
 * This keeps the entire business/processing logic in the same PHP code, and not in both templates and code.
 */


// Coding standard:
// Note how all methods are sorted by alphabetic order.
// It's weird at first sight, but allows much faster lookups for code.




/**
 * Child class of TemplateEngine.
 * Does the actual processing.
 */
class Template
{
  var $source = "";
  var $result = "";
  var $parent = null;
  var $name   = "(top level)";
  var $consumeSpacing = false;
  var $defaultEncoding = 'html';


  /**
   * Constructor, currently empty.
   */
  function Template()
  {

  }


  /**
   * Append the source contents to the result buffer.
   *
   * This is used to create lists:
   *
   * >  $block = $template->getBlock("list");
   * >  if( $block !== false ) {
   * >    foreach( $items as $item ) {
   * >      $block->appendSource();
   * >      $block->replace( 'name', $item['name'] );
   * >    }
   * >    $tpl->replaceBlock($block);  // writes result back to parent.
   * >  }
   *
   */
  function appendSource()
  {
    $this->result .= $this->source;
  }


  /**
   * Protected function to create a new block object, used by getBlock.
   * @scope protected
   */
  function &_createBlock( &$source, $name, $consumeSpacing )
  {
    // Always create sub objects of the same type.
    $class = get_class($this);
    $block = new $class();

    // Initialize the block
    $block->_initialize( $source, $name, $this, $consumeSpacing );
    return $block;
  }


  /**
   * Update a string position by consuming the leading spaces too.
   * Decrements $pos.
   */
  function _consumeLeadingSpace( &$string, &$pos )
  {
    $pos2 = $pos;

    $debug = strpos("My poor", $string) !== false;

    // Keep travering backwards while there are spaces.
    while( $pos2 > 0 )
    {
      // Peek next character.
      $pos2--;
      $char = $string{$pos2};

      // See if end of spacing was found.
      if( $char != " " && $char != "\t" )
      {
        // See if this space was at the start of the line.
        if( $string{$pos2} == "\n"
        ||  $string{$pos2} == "\r" )
        {
          $pos = ($pos2 + 1);
        }

        return;
      }
    }
  }


  /**
   * Update a string position by consuming the trailing newline too.
   * Increments $pos.
   * Return whether $pos was at the end of a line.
   */
  function _consumeTrailingSpace( &$string, &$pos )
  {
    // Remove newline after end marking
    // Consume a \r, \n, or \r followed by \n

    $length = strlen($string);
    $atNewline = ($pos == $length);

    if( ! $atNewline )
    {
      if( $string{$pos} == "\r" )
      {
        $pos++;
        $atNewline = true;
      }

      if( $pos < $length && $string{$pos} == "\n" )
      {
        $pos++;
        $atNewline = true;
      }
    }

    return $atNewline;
  }


  /**
   * Extract a block from the template source and return.
   * The block can be updated seperately and written back later.
   * This can also be used to create lists, by repeating the same block.
   */
  function getBlock( $name, $consumeSpacing = true )
  {
    // First check if there is still a block in the result.
    // It's no use to return a block that can't be replaced later.
    if( ! $this->hasBlock( $name ) )
    {
      return false;
    }

    $startTag = '<!--[[begin ' . $name . ']]-->';
    $endTag   = '<!--[[end ' . $name . ']]-->';

    // Extract a block from the source so it's always in clean unreplaced state.
    // Extracting a block from the $result buffer has some interesting effects though:
    // - it allows global replaces to occur in all blocks that haven't been extracted yet.
    // - the calling code needs to process blocks/subblocks before all global vars.

    // Get start pos
    $startPos = strpos( $this->source, $startTag );
    if( $startPos === false ) return false;
    $startPos += strlen($startTag);

    // Get end pos
    $endPos = strpos( $this->source, $endTag, $startPos );
    if( $endPos === false ) return false;

    // Remove uglyness
    if( $consumeSpacing )
    {
      // Start substr() after the newline of the end tag, avoid copying the newline.
      $this->_consumeTrailingSpace( $this->source, $startPos ); // increments startPos.
    }

    // Extract string and initialize.
    $blockSource = substr( $this->source, $startPos, $endPos - $startPos );
    if( $consumeSpacing )
    {
      $blockSource = rtrim($blockSource, "\t ");  // remove indent of end tag too, avoid repeating that with appendBlock().
    }

    // Return the data as new block
    return $this->_createBlock( $blockSource, $name, $consumeSpacing );
  }


  /**
   * Return the default encoding used for replace().
   */
  function getDefaultEncoding()
  {
    return $this->defaultEncoding;
  }


  /**
   * Return the name of the block
   */
  function getName()
  {
    return $this->name;
  }


  /**
   * Return the parent block
   */
  function &getParent()
  {
    return $this->parent;
  }


  /**
   * Return the result string of the block.
   */
  function getResult()
  {
    return $this->result;
  }


  /**
   * Return whether a given block is still present in the result.
   * This function also checks in subblocks.
   */
  function hasBlock( $name )
  {
    $startTag = '<!--[[begin ' . $name . ']]-->';
    $endTag   = '<!--[[end ' . $name . ']]-->';

    // Check start pos
    $startPos = strpos( $this->result, $startTag );
    if( $startPos === false ) return false;

    // Check end pos
    $endPos = strpos( $this->result, $endTag, $startPos );
    if( $endPos === false ) return false;

    // Block still available!
    return true;
  }


  /**
   * Return whether a given tag is present in the result.
   * This function also checks in subblocks.
   */
  function hasTag( $name )
  {
    return substr( '<!--[' . $tag . ']-->', $this->result ) !== false;
  }


  /**
   * Initialize the newly created template block.
   * @scope protected
   */
  function _initialize( $source, $name, &$parent, $consumeSpacing )
  {
    $this->source = $source;
    $this->result = "";
    $this->name   = $name;
    $this->parent =& $parent;
    $this->consumeSpacing = $consumeSpacing;

    // Also copy settings!
    $this->defaultEncoding = $parent->defaultEncoding;
  }


  /**
   * Load a file as template source.
   */
  function loadFile( $filename, $consumeSpacing = true )
  {
    // Read file data
    if( ( $source = @file_get_contents( $filename ) ) === false )
    {
      ErrorCache::setError( "Unable to load file: " . $filename );
      return false;
    }

    // Load includes
    $source = $this->_loadIncludes( $source, dirname($filename), $consumeSpacing );
    if( $source === false )
    {
      return false;
    }

    // Initialize first template block.
    $this->source = $source;
    $this->result = $source;
    return true;
  }


  /**
   * Load the includes in a template source.
   */
  function _loadIncludes( $string, $relativePath, $consumeSpacing = true )
  {
    $beginTag = '<!--[[include ';
    $endTag   = ']]-->';
    $beginLength = strlen($beginTag);
    $endLength   = strlen($endTag);

    $beginPos = 0;
    while( true )
    {
      $beginPos = strpos($string, $beginTag, $beginPos);
      if( $beginPos === false ) break;

      $endPos = strpos($string, $endTag, $beginPos + $beginLength);
      if( $endPos === false ) break;

      // Load the file.
      $filename = substr( $string, $beginPos + $beginLength, $endPos - $beginPos - $beginLength );

      // Append current path.
      if( $filename{0} != '/' )
      {
        $filename = $relativePath . '/' . $filename;
      }

      // Load file data.
      if( ( $filedata = @file_get_contents( $filename ) ) === false )
      {
        ErrorCache::setError( "Unable to load include file: " . $filename );
        return false;
      }

      // Load includes of the sub file.
      $filedata = $this->_loadIncludes( $filedata, dirname($filename), $consumeSpacing );
      if( $filedata === false )
      {
        return false;
      }

      // Move endPos to end.
      $endPos += $endLength;

      // Remove uglyness
      if( $consumeSpacing )
      {
        // Remove spacing before start marking and after end marking.
        $this->_consumeLeadingSpace( $string, $beginPos );  // decrements startPos.
        $this->_consumeTrailingSpace( $string, $endPos );   // increments endPos.
      }

      // Replace include line with file data.
      $string = substr_replace( $string, $filedata, $beginPos, $endPos - $beginPos );
      $beginPos += strlen($filedata);
    }

    return $string;
  }


  /**
   * Load a string as template source.
   */
  function loadString( $string, $relativePath = null, $consumeSpacing = true )
  {
    // Load includes, use relative from PHP file by default.
    if( $relativePath == null )
    {
      $relativePath = rtrim( dirname($_SERVER['SCRIPT_FILENAME']), '/');
    }
    $source = $this->_loadIncludes( $string, $relativePath, $consumeSpacing );
    if( $source === false )
    {
      return false;
    }

    $this->source = $source;
    $this->result = $source;
    return true;
  }


  /**
   * Remove a block from the template result.
   */
  function removeBlock( $name, $consumeSpacing = true )
  {
    $startTag = '<!--[[begin ' . $name . ']]-->';
    $endTag   = '<!--[[end ' . $name . ']]-->';

    // Get start pos
    $startPos = strpos( $this->result, $startTag, 0 );
    if( $startPos === false ) break;

    // Get end pos
    $endPos = strpos( $this->result, $endTag, $startPos );
    if( $endPos === false ) break;
    $endPos += strlen($endTag);

    // Remove uglyness
    if( $consumeSpacing )
    {
      // Remove spacing before start marking and after end marking.
      $this->_consumeLeadingSpace( $this->result, $startPos );  // decrements startPos.
      $this->_consumeTrailingSpace( $this->result, $endPos );   // increments endPos.
    }

    $this->result = substr_replace( $this->result, "", $startPos, $endPos - $startPos );
  }


  /**
   * Perform a string replacement on the tag: <!--[tag]-->.
   * This is a convenience function for the replaceTag() call.
   *
   * By default it encodes the string with htmlentities(), so XSS attacks are avoided.
   * While this template engine can be used on only text content, it will be used mostly for HTML files.
   *
   * Use setDefaultEncoding() to change the encoding.
   */
  function replace( $tag, $replacement, $encoding = null )
  {
    // Use default encoding if none is set.
    if( is_null($encoding) )
    {
      $encoding = $this->defaultEncoding;
    }

    // Apply encoding
    switch( $encoding )
    {
      case 'html':
        $replacement = nl2br( htmlentities( $replacement ) );
        break;

      case 'xml':
        $replacement = htmlspecialchars( $replacement );
        break;

      case 'url':
        $replacement = urlencode( $replacement );
        break;
    }

    // Replace tag
    $this->replaceTag( $tag, $replacement );
  }


  /**
   * Replace the result of an extracted block.
   */
  function replaceBlock( &$block )
  {
    $name = $block->getName();

    $startTag = '<!--[[begin ' . $name . ']]-->';
    $endTag   = '<!--[[end ' . $name . ']]-->';

    // Get start pos
    $startPos = strpos( $this->result, $startTag );
    if( $startPos === false ) return false;

    // Get end pos
    $endPos = strpos( $this->result, $endTag, $startPos );
    if( $endPos === false ) return false;
    $endPos += strlen($endTag);

    // Remove uglyness when it was also done with getBlock()
    if( $block->consumeSpacing )
    {
      // Remove spacing before start marking and after end marking.
      $this->_consumeLeadingSpace( $this->result, $startPos );  // decrements startPos.
      $this->_consumeTrailingSpace( $this->result, $endPos );   // increments endPos.
    }

    // Update result with block replaced.
    $this->result = substr_replace( $this->result, $block->getResult(), $startPos, $endPos - $startPos );

    // Indicate the block was replaced.
    return true;
  }


  /**
   * Perform a string replacement on the tag: <!--[tag]-->.
   * Note this function does not protect against HTML-injection.
   */
  function replaceTag( $tag, $replacement )
  {
    $this->result = str_replace( '<!--[' . $tag . ']-->', $replacement, $this->result );
  }


  /**
   * Process a group of exclusive template blocks.
   *
   * Keeps the block with the tag <!--[[begin name=value]]-->,
   * and remove all other blocks with <!--[[begin name=..]]-->
   *
   * The blocks may appear multiple times in the template.
   * Booleans are converted to 1/0.
   */
  function set( $name, $value, $consumeSpacing = true )
  {
    // Handle booleans as 1/0
    if( is_bool($value) )
    {
      $value = (int) $value;
    }
    
    // Remove the tags of the blocks to keep.
    $keepStart = '<!--[[begin ' . $name . '=' . $value . ']]-->';
    $keepEnd   = '<!--[[end ' . $name . '=' . $value . ']]-->';

    $this->result = str_replace( $keepStart, "", $this->result );
    $this->result = str_replace( $keepEnd, "", $this->result );

    // Remove all blocks that are still present.
    $commonStart = '<!--[[begin ' . $name . '=';

    if( preg_match_all( '/<!--\[\[begin (' . preg_quote($name) . '=[^\]]*)\]\]-->/', $this->result, $cap, PREG_SET_ORDER ) )
    {
      foreach( $cap as $match )
      {
        $this->removeBlock( $match[1], $consumeSpacing );
      }
    }
  }


  /**
   * Set the default encoding for replace()
   * Supported values:
   * html:  encode with htmlentities() and nl2br()
   * xml:   encode with htmlspecialchars()
   * url:   encode with urlencode()
   */
  function setDefaultEncoding( $encoding )
  {
    $this->defaultEncoding = $encoding;
  }
}


?>
