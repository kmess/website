<?php

  $_ErrorCache_errors = array();
  $_ErrorCache_backtrace = '';

  /**
   * Print an error report.
   * This is a convenience function.
   */
  function print_error_report()
  {
    print ErrorCache::getHtmlReport();
  }


  /**
   * Static class to keep track of errors.
   *
   * Makes exception handling possible without 'throw' statements.
   * The basic idea is: every function returns 'false' on error,
   * the main (gui) function calls print_error_report() or one of the static methods of ErrorCache.
   */
  class ErrorCache
  {
    /**
     * Set the error.
     */
    function setError( $message )
    {
      global $_ErrorCache_errors;
      global $_ErrorCache_backtrace;

      // Get backtrace, get calling function
      $bt = debug_backtrace();
      $function = $bt[1]['function'];
      if( strlen($bt[1]['type']) > 0 )
      {
        $function = $bt[1]['class'] . $bt[1]['type'] . $function;
      }

      // Update globals
      $_ErrorCache_errors[] = $function . ': ' . $message;
      $_ErrorCache_backtrace = $bt;
    }


    /**
     * Return all error messages.
     */
    function getMessages()
    {
      global $_ErrorCache_errors;
      return $_ErrorCache_errors;
    }


    /**
     * Return the backtrace of the last error.
     */
    function getLastBacktrace()
    {
      global $_ErrorCache_backtrace;
      return $_ErrorCache_backtrace;
    }


    /**
     * Show an error report.
     */
    function getHtmlReport()
    {
      global $_ErrorCache_errors;
      global $_ErrorCache_backtrace;

      // Show title.
      $xhtml = '<div class="errorReport" style="border: 1px solid black; background-color: #ccc; margin-left: 1em; margin-top: 1em; width: 80%; font-size: 12pt">' . "\n"
             . '<div class="errorHeader" style="background: #666699; color: #fff; font-weight: bold; padding: 2px;">' . "\n"
             .  "  An error occured while processing the request\n"
             .  '</div>'
             .  '<div class="errorMessage" style="padding: 1em 2px; background-color: #fff">' . "\n";

      // Show error stack
      foreach( $_ErrorCache_errors as $error )
      {
        $xhtml .= '<ul><li style="list-style-type: none">' . nl2br(htmlentities($error)) . "\n";
      }
      for( $i = 0; $i < count($_ErrorCache_errors); $i++ )
      {
        $xhtml .= '</li></ul>' . "\n";
      }


      // Show call stack
      $xhtml .= '</div><div class="errorStack" style="padding: 2px;">' . "\n"
              . 'Call stack:' . "\n"
              . '<ol>';
      foreach( $_ErrorCache_backtrace as $i => $item )
      {
        // Get function name
        $function = $item['function'];
        if( isset($item['type']) && strlen($item['type']) > 0 )
        {
          $function = $item['class'] . $item['type'] . $function;
        }

        // Get function args
        $args = '';
        if( count($item['args']) > 0 )
        {
          $args = ' ';
          foreach( $item['args'] as $i => $arg )
          {
            if( $i > 0 )
            {
              $args .= ', ';
            }
            if( is_bool($arg) )
            {
              $args .= $arg ? 'true' : 'false';
            }
            else if( is_null($arg) )
            {
              $args .= 'null';
            }
            if( is_numeric($arg) )
            {
              $args .= $arg;
            }
            else if( is_string($arg) )
            {
              if( strlen($arg) > 100 )
              {
                $arg = substr($arg, 0, 100) . '...';
              }
              $args .= '"' . $arg . '"';
            }
            else
            {
              $args .= $arg;
            }
          }
          $args .= ' ';
        }
        
        $xhtml .= '<li><code style="font-size: 9pt">' . htmlentities($function)
                . '(' . $args . ')</code>'
                . ' at <a href="file:' . htmlentities($item['file']) . '">' . htmlentities($item['file']) . '</a>'
                . ' line ' . $item['line'] . '</li>' . "\n";
      }

      
      // Show script arguments
      $xhtml .= '</div><div class="errorStack" style="padding: 2px;">' . "\n"
              . 'Application parameters:' . "\n";
      if( count($_GET) == 0 && count($_POST) == 0 )
      {
        $xhtml .= 'none';
      }
      else
      {
        $xhtml .= '<blockquote>' . "\n";
        foreach( $_GET as $key => $value )
        {
          $xhtml .= htmlentities($key . ': ' . $value) . '<br/>';
        }
        foreach( $_POST as $key => $value )
        {
          $xhtml .= htmlentities($key . ': ' . $value) . '<br/>';
        }
        $xhtml .= '</blockquote>' . "\n";
      }

      $xhtml .= '</div></div>';
      return $xhtml;
    }
  }


?>
