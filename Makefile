#
#  Makefile for the KMess website.
#  this file runs all commands to generate
#  the HTML content from the XML sources.
#


# Build list of files
DIRS    = $(shell find . -type d | grep -v resources | grep -v newstuff | sort)
SRCXML  = $(foreach DIR,$(DIRS),$(wildcard $(DIR)/*.xml))
FILES   = $(subst .xml,.html,$(SRCXML))
XSLDEP  = $(wildcard resources/xsl/include/*.xsl) $(wildcard resources/*.xml) resources/xsl/kmess.xsl Makefile

# Parameters for xslt processing
DATE    = $(shell date '+%Y-%m-%d %H:%M')

# News file settings
NEWS    = resources/cache/NEWS.current

# Default "all" target to compile the html files
all: $(FILES)



####################################################
#
# Standard web pages.
#
# Automatic target using pattern matching ($< = source, $@ = dest).
# The output files are made writable, so other projectmembers can update the site too
#

%.html: %.xml $(XSLDEP)
	@test -e "$@" && echo "Updating $@" || echo "Creating $@"
	@xsltproc --stringparam "date" "$(DATE)" \
	         --stringparam "path" "`dirname $@`" \
	         --nonet \
	         --nomkdir \
	         --output "$@" \
	         "resources/xsl/kmess.xsl" \
	         "$<"
	@sed -i -e 's/ xmlns=""//g' "$@"  # fixes empty xmlns="" in the code
	@chgrp kmess "$@" || true
	@chmod g+w   "$@" || true
#	@sabcmd "\$$date=$(DATE)" "\$$path=`dirname $@`" "resources/xsl/kmess.xsl" "$<" "$@"



####################################################
#
# Changelog news file.
# When the news source does not exist, download it from SVN.
#
# Re-generate changes/ folder when NEWS changed
# Generate the XML pages from the news file only if it changed.
#

ifdef NEWS
$(NEWS):
	@echo "You don't have a NEWS file. Take it from the Git repo!"
	@echo "Copy /NEWS from the branch you want to follow, to this location:"
	@echo "$(NEWS)"
	@exit 1

resources/cache/NEWS: $(NEWS)
	@echo "Updating the NEWS file..."
	@diff -q "resources/cache/NEWS" "$(NEWS)" >/dev/null || cp "$(NEWS)" "resources/cache/NEWS"

changes/index.xml: resources/cache/NEWS resources/scripts/news2xml.pl
	@echo "Running resources/scripts/news2xml.pl"
	@resources/scripts/news2xml.pl changes < $(NEWS)
	@chgrp -R kmess changes/
	@chmod -R g+w   changes/
endif



####################################################
#
# Other small targets
#

#
# 'make upload' to update the online website
#
upload: all
	@echo "Preparing to upload:"
	@find . -type f -cnewer last.upload | sort | resources/scripts/upload.pl
	@touch last.upload


#
# generate the last upload file if it didn't exist
#
last.upload:
	@touch last.upload


#
# Clean target.
# Disabled because it deletes all online HTML content
#
#clean:
#	rm -f $(FILES)


#
# explicitly declare a target as phony,
# if a file called "all" is ever created
#
.PHONY: all clean upload

