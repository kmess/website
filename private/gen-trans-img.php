<?php

header("Location: /");
exit;


# gen-trans-img.php
# Generate an image that contains the current translation status.
# Copyright (c) 2009, Sjors Gielen
# All rights reserved. Distributed under the terms of the BSD license.

// configuration
#$dir  = "/var/svn/checkout/trunk/kmess/po";
#$title = "KMess trunk";
$dir  = "/var/svn/checkout/branches/kmess/kmess-2.0.x/po";
$title = "KMess 2.0.x branch";
$bgimg = "../resources/img/background.png";
$w    = 800;
$font = '../resources/DejaVuSans.ttf'; // TODO
$header_text = "$title - Localization Status";

$languages = array();

$files = glob("$dir/*.po");

foreach( $files as $file )
{
  $lang = basename( $file, ".po" );
  $strings = `cat $file | grep '^msgid' | wc -l`;
  $untrans = `msgattrib --untranslated --no-obsolete $file | grep '^msgid' | wc -l`;
  $fuzzy   = `msgattrib --only-fuzzy --no-obsolete $file | grep '^msgid' | wc -l`;

  $strings = trim( $strings );
  $untrans = trim( $untrans );
  $fuzzy   = trim( $fuzzy );

  $languages[$lang] = array( $strings, $fuzzy, $untrans );
}

ksort($languages);

$image_height = 140 + count($languages) * 25;

$image = imagecreatetruecolor( $w, $image_height );
//imageantialias( $image, true );

// prepare colors
$black  = imagecolorallocate( $image, 0, 0, 0 );
$white  = imagecolorallocate( $image, 255, 255, 255 );
$gray2  = imagecolorallocate( $image, 150, 150, 150 );
$gray   = imagecolorallocate( $image, 200, 200, 200 );
$green  = imagecolorallocatealpha( $image, 0, 255, 0, 30 );
$yellow = imagecolorallocatealpha( $image, 255, 255, 0, 30 );
$red    = imagecolorallocatealpha( $image, 255, 0, 0, 30 );
imagefill( $image, 0, 0, $white );

$background = imagecreatefrompng( $bgimg );
$blit_x = imagesx( $image ) - imagesx( $background );
$blit_y = imagesy( $image ) - imagesy( $background );
if( $blit_x < 0 ) $blit_x = 0;
if( $blit_y < 0 ) $blit_y = 0;
imagecopy( $image, $background, $blit_x, $blit_y, 0, 0, imagesx( $background ), imagesy( $background ) );

$generatedDate = "Generated at " . date("r");
$generated_bbox = imagettfbbox( 10, 0, $font, $generatedDate );
$header_skip = $generated_bbox[1] - $generated_bbox[7];

imagettftext( $image, 8, 0, 0, 10, $gray2, $font, $generatedDate );

$bbox = imagettfbbox( 18, 0, $font, $header_text );
$bbox_width = $bbox[4] - $bbox[6];
$bbox_height = $bbox[1] - $bbox[7];
$header_skip += $bbox_height + 10;
imagettftext( $image, 18, 0, ( imagesx( $image ) - $bbox_width)/2,
  $header_skip, $black, $font, $header_text );

// Legend box
$leg_leg_bbox   = imagettfbbox( 12, 0, $font, "Legend" );
$leg_trans_bbox = imagettfbbox( 10, 0, $font, "Translated" );
$leg_fuzzy_bbox = imagettfbbox( 10, 0, $font, "Fuzzy" );
$leg_untr_bbox  = imagettfbbox( 10, 0, $font, "Untranslated" );

$leg_leg_width    = $leg_leg_bbox  [4] - $leg_leg_bbox  [6];
$leg_leg_height   = $leg_leg_bbox  [1] - $leg_leg_bbox  [7];
$leg_trans_width  = $leg_trans_bbox[4] - $leg_trans_bbox[6];
$leg_trans_height = $leg_trans_bbox[1] - $leg_trans_bbox[7];
$leg_fuzzy_width  = $leg_fuzzy_bbox[4] - $leg_fuzzy_bbox[6];
$leg_fuzzy_height = $leg_fuzzy_bbox[1] - $leg_fuzzy_bbox[7];
$leg_untr_width   = $leg_untr_bbox [4] - $leg_untr_bbox [6];
$leg_untr_height  = $leg_untr_bbox [1] - $leg_untr_bbox [7];

$leg_colorbox_width  = 20;
$leg_colorbox_height = 15;

$leg_type_width_max = $leg_trans_width;
if( $leg_fuzzy_width > $leg_type_width_max )
  $leg_type_width_max = $leg_fuzzy_width;
if( $leg_untr_width > $leg_type_width_max )
  $leg_type_width_max = $leg_untr_width;

$leg_line_height_max = $leg_colorbox_height;
if( $leg_trans_height > $leg_line_height_max )
  $leg_line_height_max = $leg_trans_height;
if( $leg_fuzzy_height > $leg_line_height_max )
  $leg_line_height_max = $leg_fuzzy_height;
if( $leg_untr_height > $leg_line_height_max )
  $leg_line_height_max = $leg_untr_height;

$leg_box_width = 5 + $leg_colorbox_width + 5 + $leg_type_width_max + 5;
if( $leg_box_width < ($leg_leg_width + 10) )
  $leg_box_width = $leg_leg_width + 10;
$leg_box_height = 5 + $leg_leg_height + ( 5 + $leg_line_height_max ) * 3 + 5;

$leg_box = imagecreate( $leg_box_width + 1, $leg_box_height + 1 );
$leg_box_white  = imagecolorallocate( $leg_box, 255, 255, 255 );
$leg_box_black  = imagecolorallocate( $leg_box, 0, 0, 0 );
$leg_box_gray   = imagecolorallocate( $leg_box, 200, 200, 200 );
$leg_box_green  = imagecolorallocatealpha( $leg_box, 0, 255, 0, 30 );
$leg_box_yellow = imagecolorallocatealpha( $leg_box, 255, 255, 0, 30 );
$leg_box_red    = imagecolorallocatealpha( $leg_box, 255, 0, 0, 30 );
imagefill( $leg_box, 0, 0, $leg_box_white );
imagerectangle( $leg_box, 0, 0, $leg_box_width, $leg_box_height, $leg_box_gray );

imagettftext( $leg_box, 12, 0, 5 + ($leg_leg_width/2), 2 + $leg_leg_height, $leg_box_black, $font, "Legend" );

$leg_fourth_colorbox_height      = ceil($leg_colorbox_height/4) - 2;
$leg_half_colorbox_height        = ceil($leg_colorbox_height/2);
$leg_trans_colorbox_height_start = 5 + $leg_leg_height + 5 + ceil($leg_trans_height/2) - $leg_fourth_colorbox_height;
$leg_fuzzy_colorbox_height_start = 5 + $leg_leg_height + 5 + $leg_trans_height + 5 + ceil($leg_fuzzy_height/2) - $leg_fourth_colorbox_height;
$leg_untr_colorbox_height_start  = 5 + $leg_leg_height + 5 + $leg_trans_height + 5 + $leg_fuzzy_height + 5 + ceil($leg_untr_height/2) - $leg_fourth_colorbox_height;
$leg_trans_colorbox_height_end   = $leg_trans_colorbox_height_start + $leg_half_colorbox_height;
$leg_fuzzy_colorbox_height_end   = $leg_fuzzy_colorbox_height_start + $leg_half_colorbox_height;
$leg_untr_colorbox_height_end    = $leg_untr_colorbox_height_start  + $leg_half_colorbox_height;

imagefilledrectangle( $leg_box, 5, $leg_trans_colorbox_height_start, 5 + $leg_colorbox_width,
                      $leg_trans_colorbox_height_end, $leg_box_green  );
imagerectangle(       $leg_box, 5, $leg_trans_colorbox_height_start, 5 + $leg_colorbox_width,
                      $leg_trans_colorbox_height_end, $leg_box_gray   );
imagefilledrectangle( $leg_box, 5, $leg_fuzzy_colorbox_height_start, 5 + $leg_colorbox_width,
                      $leg_fuzzy_colorbox_height_end, $leg_box_yellow );
imagerectangle(       $leg_box, 5, $leg_fuzzy_colorbox_height_start, 5 + $leg_colorbox_width,
                      $leg_fuzzy_colorbox_height_end, $leg_box_gray   );
imagefilledrectangle( $leg_box, 5, $leg_untr_colorbox_height_start,  5 + $leg_colorbox_width,
                      $leg_untr_colorbox_height_end, $leg_box_red     );
imagerectangle(       $leg_box, 5, $leg_untr_colorbox_height_start,  5 + $leg_colorbox_width,
                      $leg_untr_colorbox_height_end, $leg_box_gray    );

imagettftext( $leg_box, 10, 0, 10 + $leg_colorbox_width, 5 + $leg_leg_height + 5 + $leg_trans_height, $leg_box_black, $font, "Translated" );
imagettftext( $leg_box, 10, 0, 10 + $leg_colorbox_width, 5 + $leg_leg_height + 5 + $leg_trans_height + 5 + $leg_fuzzy_height,
  $leg_box_black, $font, "Fuzzy" );
imagettftext( $leg_box, 10, 0, 10 + $leg_colorbox_width, 5 + $leg_leg_height + 5 + $leg_trans_height + 5 + $leg_fuzzy_height + 5 +
  $leg_untr_height, $leg_box_black, $font, "Untranslated" );

imagecopy( $image, $leg_box, imagesx($image) - 5 - imagesx($leg_box), 5, 0, 0, imagesx($leg_box), imagesy($leg_box) );

if( $header_skip < ( imagesy($leg_box) + 20 ) )
  $header_skip = imagesy( $leg_box ) + 20;

imagedestroy( $leg_box );

// Create language string labels and compute the max width of the labels
$max_lang_width = 0;
$max_trans_width = 0;
$languagestrings = array();
$langlabelstrings = array();
$comma_bbox = imagettfbbox( 10, 0, $font, ", " );
$comma_width = $comma_bbox[4] - $comma_bbox[6];
foreach( $languages as $name => $value )
{
  $lang_bbox  = imagettfbbox( 10, 0, $font, $name );
  $trans_bbox = imagettfbbox( 10, 0, $font, $value[0] - $value[1] - $value[2] );
  $fuz_bbox   = imagettfbbox( 10, 0, $font, $value[1] );
  $untr_bbox  = imagettfbbox( 10, 0, $font, $value[2] );

  $lang_width   = $lang_bbox [4] - $lang_bbox [6];
  $lang_height  = $lang_bbox [1] - $lang_bbox [7];
  $trans_width  = $trans_bbox[4] - $trans_bbox[6];
  $trans_height = $trans_bbox[1] - $trans_bbox[7];
  $fuz_width    = $fuz_bbox  [4] - $fuz_bbox  [6];
  $fuz_height   = $fuz_bbox  [1] - $fuz_bbox  [7];
  $untr_width   = $untr_bbox [4] - $untr_bbox [6];
  $untr_height  = $untr_bbox [1] - $untr_bbox [7];

  $full_trans_width = $trans_width + $comma_width + $fuz_width + $comma_width + $untr_width;

  $lang_width += $comma_width;
  if( $max_lang_width < $lang_width )
    $max_lang_width = $lang_width;
  if( $max_trans_width < $full_trans_width )
    $max_trans_width = $full_trans_width;

  // Find max needed height
  $full_label_height = $lang_height;
  if( $trans_height > $full_label_height )
    $full_label_height = $trans_height;
  if( $fuz_height > $full_label_height )
    $full_label_height = $fuz_height;
  if( $untr_height > $full_label_height )
    $full_label_height = $untr_height;

  $language_image = imagecreate( $lang_width, $full_label_height );
  $lang_white  = imagecolorallocate( $language_image, 255, 255, 255 );
  $lang_black  = imagecolorallocate( $language_image, 0, 0, 0 );
  imagefill( $language_image, 0, 0, $lang_white );
  imagettftext( $language_image, 10, 0, 0, 10, $lang_black, $font, $name );

  $langlabel_image = imagecreate( $full_trans_width, $full_label_height );
  $langlbl_white  = imagecolorallocate( $langlabel_image, 255, 255, 255 );
  $langlbl_black  = imagecolorallocate( $langlabel_image, 0, 0, 0 );
  $langlbl_green  = imagecolorallocatealpha( $langlabel_image, 0, 150, 0, 30 );
  $langlbl_yellow = imagecolorallocatealpha( $langlabel_image, 150, 150, 0, 30 );
  $langlbl_red    = imagecolorallocatealpha( $langlabel_image, 255, 0, 0, 30 );
  imagefill( $langlabel_image, 0, 0, $langlbl_white );
  imagettftext( $langlabel_image, 10, 0, 0, 10, $langlbl_green, $font, $value[0] - $value[1] - $value[2] );
  imagettftext( $langlabel_image, 10, 0, $trans_width, 10, $langlbl_black, $font, ", " );
  imagettftext( $langlabel_image, 10, 0, $trans_width + $comma_width, 10, $langlbl_yellow, $font, $value[1] );
  imagettftext( $langlabel_image, 10, 0, $trans_width + $comma_width + $fuz_width, 10, $langlbl_black,
                $font, ", " );
  imagettftext( $langlabel_image, 10, 0, $trans_width + $comma_width + $fuz_width + $comma_width, 10,
                $langlbl_red, $font, $value[2] );

  $languagestrings[$name] = $language_image;
  $langlabelstrings[$name] = $langlabel_image;
}


// paint left part of the image: language names and labels
$language_paint_y = $header_skip + 20; // skip the top
foreach( $languagestrings as $name => $lang_image )
{
  $langlabel = $langlabelstrings[$name];

  $blit_x = $max_lang_width - imagesx($lang_image);
  imagecopy( $image, $lang_image, $blit_x, $language_paint_y + 5, 0, 0,
    imagesx( $lang_image ), imagesy( $lang_image ) );

  $blit_x = $max_lang_width;
  imagecopy( $image, $langlabel, $blit_x, $language_paint_y + 5, 0, 0,
    imagesx( $langlabel), imagesy( $langlabel ) );

  $language_paint_y += 25;
}

// paint axes
$barwidth = $w - 30 - $max_lang_width - $max_trans_width - 2;
$hundredpercent = $barwidth;
$fiftypercent   = $hundredpercent / 2;
$twentyfivepercent  = $fiftypercent / 2;
$seventyfivepercent = $fiftypercent + $twentyfivepercent;

$left               = $max_lang_width + $max_trans_width + 2;
$twentyfivepercent  += $left;
$fiftypercent       += $left;
$seventyfivepercent += $left;
$hundredpercent     += $left;

// left axis
$top = $header_skip + 10;
imageline( $image, $left, $top, $left, $language_paint_y, $gray );
// top axis
imageline( $image, $left, $top, $hundredpercent, $top, $gray );
// bottom axis
imageline( $image, $left, $language_paint_y, $hundredpercent, $language_paint_y, $gray );
// percentage lines
imageline( $image, $twentyfivepercent,  $top, $twentyfivepercent,  $language_paint_y, $gray );
imageline( $image, $fiftypercent,       $top, $fiftypercent,       $language_paint_y, $gray );
imageline( $image, $seventyfivepercent, $top, $seventyfivepercent, $language_paint_y, $gray );
imageline( $image, $hundredpercent,     $top, $hundredpercent,     $language_paint_y, $gray );

// percentage labels
$label_y = $header_skip;
imagettftext( $image, 10, 0, $left-10, $label_y, $black, $font, "0%" );
imagettftext( $image, 10, 0, $twentyfivepercent-10,  $label_y, $black, $font, "25%" );
imagettftext( $image, 10, 0, $fiftypercent-10,       $label_y, $black, $font, "50%" );
imagettftext( $image, 10, 0, $seventyfivepercent-10, $label_y, $black, $font, "75%" );
imagettftext( $image, 10, 0, $hundredpercent-15,     $label_y, $black, $font, "100%" );

// and then the bars!
$language_paint_y = $header_skip + 20;
foreach( $languages as $name => $status )
{
  // $barwidth = 100%, 0 = 0%
  $strings = $status[0];
  $fuzzy   = $status[1];
  $untrans = $status[2];

  $trans    = $strings - $fuzzy - $untrans;
  $transw   = $trans   / $strings;
  $fuzzyw   = $fuzzy   / $strings;
  $untransw = $untrans / $strings;

  $y1 = $language_paint_y + 2;
  $y2 = $language_paint_y + 18;
  $x1 = $left; // left of translated
  $x2 = $x1 + $transw   * $barwidth; // translated-fuzzy
  $x3 = $x2 + $fuzzyw   * $barwidth; // fuzzy-untranslated
  $x4 = $x3 + $untransw * $barwidth; // right of untranslated

  $bordercolor = $gray;
  if( $trans > 0 )
  {
    imagefilledrectangle( $image, $x1, $y1, $x2, $y2, $green );
    imagerectangle( $image, $x1, $y1, $x2, $y2, $bordercolor );
  }
  if( $fuzzy > 0 )
  {
    imagefilledrectangle( $image, $x2, $y1, $x3, $y2, $yellow );
    imagerectangle( $image, $x2, $y1, $x3, $y2, $bordercolor );
  }
  if( $untrans > 0 )
  {
    imagefilledrectangle( $image, $x3, $y1, $x4, $y2, $red );
    imagerectangle( $image, $x3, $y1, $x4, $y2, $bordercolor );
  }

  $language_paint_y += 25;
}

if( !headers_sent() )
{
  header("Content-Type: image/png");
  imagepng( $image );
}
imagedestroy( $image );
