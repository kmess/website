#!/bin/sh

#
# Settings or local paths
#
# you should add this line to /etc/rc.local:
# perl -e 'while(<>){ if(/^LOCKFILE\s*=\s*(.+\S)\s*$/) { unlink$1;exit } }' /etc/cron.daily/sync-kmess-svn
# (and obviously have `perl` installed and this file at that location)
# LOCKFILE must be an absolute path, by the way
LOCKFILE=/tmp/kmess-sync-lock
LOCAL_TRAC=/srv/trac/kmess
LOCAL_SVN=/var/svn/kmess
LOCAL_SVN_CO=/var/svn/checkout
LOCAL_GIT=/var/svn/kmess.git
LOCAL_GIT_CO=/var/svn/checkout_git
WEB_ROOT=/var/www/kmess.org/htdocs/
WEB_NEWS=$WEB_ROOT/resources/cache/NEWS.current
DOXY_SOURCE=$LOCAL_SVN_CO/trunk/kmess
DOXY_ROOT=$WEB_ROOT/docs

if [ -e $LOCKFILE ]; then
  echo "Warning: Won't update KMess SVN, lock file is present. Exiting.";
  exit;
fi

touch $LOCKFILE

#
# Download the kmess-svn from sourceforge to a local copy.
#
rsync -aq --timeout=300 --contimeout=300 "rsync://kmess.svn.sourceforge.net/svn/kmess/" "$LOCAL_SVN" 
# and update the local checkout
svn up $LOCAL_SVN_CO >/dev/null
# Git *mirror* updating is now done every 5 minutes by the CIA.vc committer
# see /var/svn/updatemirror.sh
# We just update the git checkout for the NEWS updater below.
cd $LOCAL_GIT_CO
# While the KMess server runs a version of git where --quiet doesn't
# really mean anything, things like this are necessary.
git pull --quiet >/dev/null 2>&1 || echo "Warning: Git pull failed"

#
# Update the Trac installation
#
cd $LOCAL_TRAC
trac-admin . resync >/dev/null 2>&1

#
# Update the news page online automatically
#
rm $WEB_NEWS
cp ${LOCAL_GIT_CO}/NEWS $WEB_NEWS
# update the website checkout
cd $WEB_ROOT
git pull --quiet >/dev/null 2>&1 || echo "Warning: Git pull failed"
make > /dev/null
make > /dev/null  # needed twice for new folders.

#
# And update the KMess documentation
#
rm -f $DOXY_ROOT/last.log
cd $DOXY_SOURCE
# Don't catch stderr output, we want to be informed of errors
doxygen $DOXY_ROOT/Doxyfile >$DOXY_ROOT/last.log

rm $LOCKFILE
