<html>
<head>
<title>Trac's SVN update</title>
</head>

<body>
<p>This script executes directly the svn update script, so that when you
run it, you will be able to browse even the latest commits in Trac's
timeline.</p>
<p>It will also update the <a href="/docs/">documentation</a>.
<p>Script output (if any):</p>
<pre>
any
<?php

// system( 'sudo /etc/cron.hourly/sync-kmess-svn 2>&1' );

?>
</pre>
</body>

</html>
