<?xml version='1.0'?>

<page>
  <meta>
    <title>News archive</title>
  </meta>

  <section>

    <title>KMess project info</title>
    <par>
      February 9 2017 -- Six years after the last announcement, we would like
      to bring back to life, if only for historical reasons, the KMess website.
    </par>
    <par>
      The shutdown of the MSN Messenger service's servers unfortunately lead
      to the closure of this project a few years back.
    </par>
    <par>
      To the former development, testing and translation teams, and all of our
      awesome contributors: THANK YOU.
    </par>
    <par>
      We had something great. We hope you all had a great time working on, and
      most importantly using, our software.
    </par>
    <par>
      So long, and thanks for all the fish.
      -Valerio
    </par>
  </section>

  <section>

    <title>KMess 2.0.6 released</title>
    <par>
      February 5 2011 -- The KMess team announces the availability of a new
      KMess point release: KMess 2.0.6, the newest version of the Live
      Messenger (MSN) client, powered by the KDE platform. As usual with our
      2.0 point releases, it contains a lot of bug fixes, but also general
      improvements, and even some new features. Here is a quick highlight of
      the changes in this new version:
    </par>

    <list>
        <item>Added tabs to the Contact Added User dialog.</item>
        <item>Added a fading effect when switching between the initial view
              and the contact list.</item>
        <item>Fixed login issues due to changes in the MSN protocol.</item>
        <item>Fixed possible crashes with the Contact Added User dialog.</item>
        <item>Fixed retrieval of display pictures from the online MSN storage.</item>
        <item>Various other smaller fixes...</item>
    </list>

    <par>
      You can find the source code at <link href="/download/">the downloads page</link>,
      and binary packages for various distributions will start flooding in soon.
    </par>
    <par>
      Thanks to 28 volunteer translators, KMess is available in 19 languages.
      Other people, without whom this release would not be what it is today,
      as well as the current members of the KMess team,
      are listed on our <link href="/people/">People page</link>.
    </par>
    <par>
      Any remarks, notes, threats, treats, as well as bug reports can be posted at <link href="#board">our forums</link>.
      You can also send us messages using the LikeBack client built into KMess,
      accessible via Help -&gt; Send a Comment to the Developers.
      We wish you a lot of fun and chatting with KMess!
    </par>
    <par>
      <link href="/changes/2.0.6/">see all changes...</link>
    </par>
  </section>

  <section>

    <title>KMess 2.0.5 released</title>
    <par>
      October 25 2010 -- The KMess team announces today the availability of the
      latest release of KMess: KMess 2.0.5, the newest version of the Live
      Messenger (MSN) client, powered by the KDE platform. As usual with our
      2.0 point releases, it contains a lot of bug fixes, but also general
      improvements, and even some new features. Here is a quick highlight of
      the changes in this new version:
    </par>

    <list>
        <item>KMess now displays your previous conversation with a contact in
              the chat window.</item>
        <item>Significantly increased speed whilst loading chat history.</item>
        <item>Added a calendar feature to the chat history dialog.</item>
        <item>Added a button to the chat window to quickly access the chat
              history of contacts in the conversation.</item>
        <item>Added notifications for address book changes (contact added,
              removed, blocked, etc).</item>
        <item>Updated the Traditional Chinese translation.</item>
        <item>Updated the Estonian translation.</item>
        <item>Updated the Hungarian translation.</item>
        <item>Updated the French translation.</item>
        <item>Fix a crash bug that occurred during offline message parsing.</item>
        <item>Various other smaller fixes...</item>
    </list>

    <par>
      You can find the source code at <link href="/download/">the downloads page</link>,
      and binary packages for various distributions will start flooding in soon.
    </par>
    <par>
      Thanks to 28 volunteer translators, KMess is available in 19 languages.
      Other people, without whom this release would not be what it is today,
      as well as the current members of the KMess team,
      are listed on our <link href="/people/">People page</link>.
    </par>
    <par>
      Any remarks, notes, threats, treats, as well as bug reports can be posted at <link href="#board">our forums</link>.
      You can also send us messages using the LikeBack client built into KMess,
      accessible via Help -&gt; Send a Comment to the Developers.
      We wish you a lot of fun and chatting with KMess!
    </par>
    <par>
      <link href="/changes/2.0.5/">see all changes...</link>
    </par>
  </section>

  <section>

    <title>KMess 2.0.4 released</title>
    <par>
      July 26 2010 -- The KMess team announces today the
      availability of the latest release of KMess: KMess 2.0.4, the newest
      version of the Live Messenger (MSN) client, powered by the KDE platform.
      As usual with our 2.0 point releases, it contains a lot of bug fixes, but
      also general improvements, and even some new features. Here is a quick
      highlight of the changes in this new version:
    </par>

    <list>
        <item>Added a small toggle button on the contact list to quickly enable/disable the "Now Listening" feature.</item>
        <item>Implemented the new KDE4 system tray specification.</item>
        <item>Added ability to drag-and-drop ink drawings from the conversation into the ink editor in order to be edited again.</item>
        <item>Fixed bug which caused KMess to fail to connect, instead giving an "Invalid Command Parameter" error.</item>
        <item>Fixed bug which caused the user's display picture to be reset upon login.</item>
        <item>Fixed bug where status was not changed from Idle upon wake up from sleep.</item>
        <item>Updated many translations.</item>
        <item>And various other smaller updates and fixes...</item>
    </list>

    <par>
      You can find the source code at <link href="/download/">the downloads page</link>,
      and binary packages for various distributions will start flooding in soon.
    </par>
    <par>
      Thanks to 26 volunteer translators, KMess is available in 19 languages.
      Other people, without whom this release would not be what it is today,
      as well as the current members of the KMess team,
      are listed on our <link href="/people/">People page</link>.
    </par>
    <par>
      Any remarks, notes, threats, treats, as well as bug reports can be posted at <link href="#board">our forums</link>.
      You can also send us messages using the LikeBack client built into KMess,
      accessible via Help -&gt; Send a Comment to the Developers.
      We wish you a lot of fun and chatting with KMess!
    </par>
    <par>
      <link href="/changes/2.0.4/">see all changes...</link>
    </par>
  </section>

  <section>

    <title>KMess 2.0.3 released</title>
    <par>
      March 10 2010 -- The KMess team announces today the immediate
      availability of our latest point release: KMess 2.0.3, the newest
      version of a Live Messenger (MSN) client powered by the KDE platform.
      It contains a slew of bug fixes, general improvements, but also some
      nice new features:
    </par>

    <list>
      <item>Drag-and-drop an image to your display picture to change it.</item>
      <item>Set your friendly name and personal message by clicking on their labels in the contact list.</item>
      <item>Significantly improved performance when drawing complex inks (in isf-Qt).</item>
      <item>Show unread email count in the system tray tooltip.</item>
      <item>Improved proxy support.</item>
      <item>Set the personal message using DBus.</item>
      <item>Replaced QCA with GCrypt.</item>
      <item>Fixed crash that sometimes occurred when logging in with the wrong password.</item>
      <item>Fixed bug which caused the "Contact Added User" dialog to appear at next logon after deleting a contact.</item>
      <item>Fixed bug which caused the chat window size to not be saved under KDE 4.4.</item>
      <item>Updated various translations.</item>
      <item>And various other smaller updates and fixes...</item>
    </list>

    <par>
      In addition, the following translations were added:
    </par>

    <list>
      <item>Added a Russian translation by Dmitriy Simbiriatin.</item>
      <item>Added a Portuguese translation by Bruno Almeida.</item>
    </list>

    <par>
      You can find the source code at <link href="/download/">the downloads page</link>,
      and binary packages for various distributions will start flooding in soon.
    </par>
    <par>
      Thanks to 25 volunteer translators, KMess is available in 19 languages.
      Other people, without whom this release would not be what it is today,
      as well as the current members of the KMess team,
      are listed on our <link href="/people/">People page</link>.
    </par>
    <par>
      Any remarks, notes, threats, treats, as well as bug reports can be posted at <link href="#board">our forums</link>.
      You can also send us messages using the LikeBack client built into KMess,
      accessible via Help -&gt; Send a Comment to the Developers.
      We wish you a lot of fun and chatting with KMess!
    </par>
    <par>
      <link href="/changes/2.0.3/">see all changes...</link>
    </par>
  </section>

  <section>
    <title>KMess 2.0.2 released</title>
    <par>
      December 5 2009 -- The KMess team announces today the immediate availability of
      our second point release: KMess 2.0.2. It is a small bugfix release for a bug
      introduced just before the release of KMess 2.0.1, making it impossible to log
      in twice in the same KMess session. Also, these translations were updated:
    </par>

    <list>
      <item>Traditional Chinese, by Yen-chou Chen.</item>
      <item>Brazilian Portuguese, by Maurício Arozi Moraes.</item>
      <item>Japanese, by Daniel E. Moctezuma.</item>
    </list>

    <par>
      You can find the source code at <link href="/download/">the downloads page</link>,
      and binary packages for various distributions will start flooding in soon.
    </par>
    <par>
      Thanks to 25 volunteer translators, KMess is available in 16 languages.
      Other people, without whom this release would not be what it is today,
      as well as the current members of the KMess team,
      are listed on our <link href="/people/">People page</link>.
    </par>
    <par>
      Any remarks, notes, threats, treats, as well as bug reports can be posted at <link href="#board">our forums</link>.
      You can also send us messages using the LikeBack client built into KMess,
      accessible via Help -&gt; Send a Comment to the Developers.
      We wish you a lot of fun and chatting with KMess!
    </par>
    <par>
      <link href="/changes/2.0.2/">see all changes...</link>
    </par>
  </section>

  <section>
    <title>KMess 2.0.1 released</title>
    <par>
      November 30 2009 -- The KMess team announces today the immediate availability of
      our first point release: KMess 2.0.1. It contains a slew of bug fixes, general improvements,
      but also some nice new features:
    </par>

    <list>
      <item>added full handwriting support using our new ISF-Qt library.</item>
      <item>added Roaming support for display picture, friendly name and personal message.</item>
      <item>added ability to drag-and-drop an image to a chat, to send it.</item>
      <item>added chat window buttons to quickly change message font and color.</item>
      <item>added a "Log in automatically" checkbox to the initial view.</item>
      <item>fixed pixelated downscaling of pictures in the contact list?</item>
      <item>fixed multiple notification problems.</item>
      <item>fixed losing the friendly name on login.</item>
      <item>fixed some crashes and memory leaks.</item>
      <item>improved the emoticons settings.</item>
      <item>the transfers window now appears in the background instead of minimized.</item>
      <item>the minimum size of the message editor is now one line of text.</item>
      <item>and other smaller updates and fixes.</item>
    </list>

    <par>
      <link href="/screenshots/">The screenshots page</link> provides a detailed overview of how KMess looks like.
    </par>
    <par>
      You can find the source code at <link href="/download/">the downloads page</link>,
      and binary packages for various distributions will start flooding in soon.
    </par>
    <par>
      Thanks to 25 volunteer translators, KMess is available in 16 languages.
      Other people, without whom this release would not be what it is today,
      as well as the current members of the KMess team,
      are listed on our <link href="/people/">People page</link>.
    </par>
    <par>
      Any remarks, notes, threats, treats, as well as bug reports can be posted at <link href="#board">our forums</link>.
      You can also send us messages using the LikeBack client built into KMess,
      accessible via Help -&gt; Send a Comment to the Developers.
      We wish you a lot of fun and chatting with KMess!
    </par>
    <par>
      <link href="/changes/2.0.1/">see all changes...</link>
    </par>
  </section>

  <section>
    <title>KMess 2.0 released</title>
    <par>
      July 24 2009 --
      The KMess team announces today the immediate availability of KMess 2.0.
      This is the first stable release of the KMess 2 series, in development since January 2008,
      one and a half years ago. KMess 2.0 is our newest, best, most fresh release yet,
      and everybody is encouraged to upgrade to it.
    </par>
    <par>
      The most obvious difference between our last stable release, KMess 1.5.2, and this release
      is the transition to KDE 4. Other than that, these things have changed:
    </par>
    <list>
      <item>added ability to re-use previously opened chat windows after reconnecting.</item>
      <item>added a dark chat style suitable for use with dark KDE color themes.</item>
      <item>added a DBus remote control interface, to enable interaction with KMess from other applications.</item>
      <item>added application-wide settings dialog, which includes an account manager.</item>
      <item>added a contact search box in the contact list.</item>
      <item>added automatic reconnection after unwanted disconnections, keeping the previous status.</item>
      <item>added connecting over HTTP, to deal with corporate firewalls which only allow connections to browse the web.</item>
      <item>added contact emoticon blacklist, to block annoying custom emoticons from contacts.</item>
      <item>added a contact list exporting dialog.</item>
      <item>added options to copy a contact's email, name, message, listened music and links present in the name/message.</item>
      <item>added detection of network connection availability, using Solid.</item>
      <item>added fading effect for long text labels and the contact list.</item>
      <item>added fast-retype of previous sentences using Ctrl+Up/Ctrl+Down.</item>
      <item>added full MSN Plus text-formatting support in chat and in the contact list.</item>
      <item>added group selection box to the "add contact" dialog.</item>
      <item>added IRC like command handling in the chat window (/away, etc..)</item>
      <item>added KWallet support to store account passwords in a secure way.</item>
      <item>added Likeback support to collect user feedback, ported from Basket of KDE 3.</item>
      <item>added chat logs history dialog.</item>
      <item>added full offline messaging support.</item>
      <item>added option to choose a directory where to put all received files.</item>
      <item>added option to choose a previously set display picture for the account.</item>
      <item>added option to choose the browser and e-mail client used to open Web links.</item>
      <item>added option to choose the dimensions of the pictures in the contact list.</item>
      <item>added option to choose the interval of ports used for fast file transfers.</item>
      <item>added option to disable displaying received winks.</item>
      <item>added option to display all offline contacts in a single group ("mixed" group mode).</item>
      <item>added option to hide currently empty groups.</item>
      <item>added support to configure the application's tool bars and menus.</item>
      <item>added support to send and receive handwritten messages (still doesn't work with some Messengers).</item>
      <item>added optional tabbed chatting support.</item>
      <item>added a contact properties dialog.</item>
      <item>added KDE proxy support.</item>
      <item>fixed appearance when using right-to-Left languages, like Arabic.</item>
      <item>fixed bug preventing temporary accounts to be correctly cleaned up.</item>
      <item>fixed chat window focus issues.</item>
      <item>fixed disconnections after many hours of use.</item>
      <item>fixed displaying duplicated status messages.</item>
      <item>improved appearance of the contact list when antialiasing is enabled.</item>
      <item>improved chat logging support, now allowing saving chats as files in text or HTML format.</item>
      <item>improved chat notifications to be compatible with the new Plasma notifications of KDE 4.2.</item>
      <item>improved client name detection of contacts.</item>
      <item>improved chat window appearance with movable panels.</item>
      <item>improved protocol support, upgraded the server core protocol to MSNP15.</item>
      <item>improved reporting of error messages, using notifications for warnings.</item>
      <item>improved the file transfer messages within the chat.</item>
      <item>fixed a lot of other bugs and made numerous other improvements.</item>
    </list>

    <par>
      <link href="/screenshots/">The screenshots page</link>, provides a detailed
      overview of the larger changes in KMess 2.0, as opposed to KMess 1.5.2.
    </par>
    <par>
      You can find the source code at <link href="/download/">the downloads page</link>,
      and binary packages for various distributions  will start flooding in soon.
    </par>
    <par>
      Thanks to 44 volunteer translators, KMess is available in 14 languages. Other people,
      without whom this release would not be what it is today, as well as the current members
      of the KMess team, are listed on our <link href="/people/">People page</link>.
    </par>
    <par>
      Any remarks, notes, declarations of love as well as bug reports can be posted at
      <link href="#board">our forums</link>,
      You can also send us messages using the LikeBack client built into KMess, accessible via
      <command>Help -> Send a Comment to the Developers</command>.
    </par>
    <par>
      We wish you a lot of fun and chatting with our best release of KMess ever!
    </par>
    <par>
      <link href="/changes/2.0/">see all changes...</link>
    </par>
  </section>

  <section>
    <title>KMess 2.0 Release Candidate 1 released</title>
    <par>
      July 17 2009 --
      The KMess team announces today the immediate availability
      of KMess 2.0 Release Candidate 1.
    </par>
    <par>
      This pre-release version is meant as a final round of testing before the final
      release of the stable 2.0 version: you are encouraged to test it and spread
      the word, to help us make the final release even better!
    </par>
    <par>
      Here is an overview of the improvements since beta 2:
    </par>
    <list>
      <item>Added display of emoticons in presence messages.</item>
      <item>Added status restoring on reconnection.</item>
      <item>Added fading effect for long text labels and the contact list.</item>
      <item>Changed the image used for unknown display pictures.</item>
      <item>Changed the application windows icons.</item>
      <item>Changed the system tray icon with one showing both the bird and the current status.</item>
      <item>Fixed a crash triggered by clients aborting while we're sending data.</item>
      <item>Fixed appearance when using right-to-Left languages, like Arabic.</item>
      <item>Fixed bug preventing temporary accounts to be forgotten.</item>
      <item>Fixed file selection errors in chat logging.</item>
      <item>Fixed newly added contacts appearing as not having added the user in their list.</item>
      <item>Improved appearance of the contact list when antialiasing is enabled.</item>
      <item>Removed all experimental features which could not be finished in time.</item>
      <item>A lot of new translations.</item>
    </list>
    <par>
      The page with <link href="/screenshots/kmess2.0beta/">2.0 screenshots</link>
      provides an detailed overview of the changes in KMess 2.0.
    </par>
    <par>
      If KMess has not been translated in your language yet, or the translation
      is incomplete, *now* is the time to get it right! Contact us at our forums
      or read <link href="#trac">our translation guide</link>.
    </par>
    <par>
      More than ever, we would like to invite you to try KMess and tell us what
      you think. Have you noticed a bug, a crash or something that
      could be improved? Contact us at our forums at
      <link href="#board">http://www.kmess.org/board</link> or, alternatively, use
      LikeBack which is available from the Help menu or the buttons in the
      top-right corner of every KMess window.
    </par>
    <par>
      <link href="/changes/2.0-rc1/">see all changes...</link>
    </par>
  </section>

  <section>
    <title>KMess 2.0 beta 2 released</title>
    <par>
      May 10, 2009 --
      As development goes on, today we are releasing
      KMess 2.0 beta 2, the second testing release of KMess.
    </par>
    <par>
      Here is an overview of the most evident improvements since beta 1:
    </par>
    <list>
      <item>fixed disconnections after many hours of use.</item>
      <item>fixed losing KDE Wallet passwords.</item>
      <item>fixed typing notifications not appearing in WLM 2009.</item>
      <item>fixed resending messages already sent as offline messages.</item>
      <item>fixed searching for offline contacts.</item>
      <item>fixed time of arrival of offline messages.</item>
      <item>fixed contact list group sorting.</item>
      <item>fixed many memory leaks.</item>
      <item>improved the Chat History dialog and its integration with KMess.</item>
      <item>improved drag and drop support in contact list.</item>
      <item>improved handwriting message sending.</item>
      <item>improved the file transfers window's behavior.</item>
      <item>improved chat session management.</item>
      <item>updated English handbook.</item>
      <item>10 updated translations.</item>
      <item>Countless more bugs fixed.</item>
    </list>
    <par>
      If KMess has not been translated in your language yet, or the translation
      is incomplete, *now* is the time to get it right! Contact us at our forums
      or read <link href="#trac">our translation guide</link>.
    </par>
    <par>
      More than ever, we would like to invite you to try KMess and tell us what
      you think. Have you noticed a bug, a crash or something that
      could be improved? Contact us at our forums at
      <link href="#board">http://www.kmess.org/board</link> or, alternatively, use
      LikeBack which is available from the Help menu or the buttons in the
      top-right corner of every KMess window.
    </par>
    <par><link href="/changes/2.0-beta2/">see all changes...</link></par>
  </section>

  <section>
    <title>KMess 2.0 beta 1 released</title>
    <par>
      March 23, 2009 --
      Today we proudly announce the first beta of KMess 2. KMess 2 beta 1
      brings MSN support for KDE 4 to a new level! In this release we've
      added a lot of new features.
    </par>
    <par>
      We've also put a lot of effort in polishing things up. Thanks to
      the help of our beta testers we've been able to iron out a lot of
      issues. If anything isn't working for you, please let us know!
    </par>
    <par><link href="/changes/2.0-beta1/">see all changes...</link></par>
  </section>

  <section>
    <title>KMess 1.5.2 released</title>
    <par>
      March 22, 2009 --
      While the development of KMess 2 is fully underway, the most useful things
      have been backported to a 1.5.2 release for KDE 3 users.
    </par>
    <par>
      Release highlights:
    </par>
    <list>
      <item>Improved support for Windows Live Messenger 2009.</item>
      <item>Improved offline messages.</item>
      <item>Improved opening links in a webbrowser (especially for Gnome users).</item>
      <item>Updated some translations.</item>
      <item>Various bugfixes and smaller enhancements.</item>
    </list>
    <par><link href="/changes/1.5.2/">see all changes...</link></par>
  </section>

  <section>
    <title>KMess 2.0 alpha 2 released</title>
    <par>
      August 10, 2008 --
      We're proud to announce the second preview release of KMess 2.
      Since the first preview release, we've worked hard to add tons of
      new exciting features to KMess, and also to improve its stability.
    </par>
    <par>
      The features we've already added are starting to mature, and since
      we never stop, we have already started to add new ones just after
      this release!
    </par>
    <par>
      As with the first alpha, likewise we're using alpha 2 as our main
      client already, but there might still be some issues. Please, test
      this alpha release and tell us how it's working for you!
    </par>
    <par><link href="/changes/2.0-alpha2/">see all changes...</link></par>
  </section>

  <section>
    <title>KMess 2.0 alpha released</title>
    <par>
      May 11, 2008 --
      We're happy to announce the first preview release of KMess 2.
      Over the past 4 months we've been really busy porting KMess to KDE 4
      and making it stable again. This release marks the end of those porting efforts.
    </par>
    <par>
      Currently, KMess 2 still looks much like KMess 1.5.1 but is run as a Qt 4 / KDE 4 application.
      The real development of new features, like support for msn-http and webcams,
      is just about to begin. Some features, like receiving hand-written messages
      and fast retype of previous sentences, have been added already.
    </par>
    <par>
      We're using it as our main client already, but there might still be some issues lurking.
      Please test this alpha release and tell us how it's working for you!
    </par>
    <par><link href="/changes/2.0-alpha/">see all changes...</link></par>
  </section>

  <section>
    <title>KMess 1.5.1 released</title>
    <par>
      April 6, 2008 --
      A new version of KMess has been released.
      This is a maintenance release, that fixes annoying bugs found in the previous KMess release.
      The most obvious fix is the picture browse button, which didn't work in our provided Autopackage.
    </par>
    <par>
      We've also added some minor features.
      Contact pictures can be displayed the contact list too,
      and the chat window gained support for spell checking.
      More information can be found in the <link href="/changes/1.5.1/">changelog</link>.
    </par>
    <par>
      Meanwhile, the development of KMess 2 is also making good progress.
      The porting to KDE 4 is complete and we're proceeding by implementing
      the new planned features for KMess 2.
    </par>
  </section>

  <section>
    <title>KMess 1.5 released</title>
    <par>
      January 9, 2008 --
      The KMess development team is very pleased to announce version 1.5 of KMess, a MSN Messenger client for KDE!
    </par>
    <par>
      After a long time of development from version 1.4 first, and the 1.5-pre series after,
      we've been able to obtain a very stable and feature-filled version.
      This will also mark a great moment: KMess 1.5 will be the last version based on Qt 3 and KDE 3.
      The next major version will be based on Qt 4 / KDE 4,
      and we expect more exciting improvements in the near future.
    </par>
    <par>
      Release highlights:
    </par>
    <list>
      <item>reliable fast file transfers</item>
      <item>automatic download of display pictures</item>
      <item>full support for custom emoticons</item>
      <item>Native KDE-styled balloon notification popups</item>
      <item>Right-click options for links and emoticons in the chat window</item>
    </list>
    <par>
      Users of KMess 1.5-pre2 will might notice that file transfers have become stable while operating full-speed.
      The chat windows received some final love and the notification ballons got a total make over
      to make use of native KDE popups. We also added the ability to send custom emoticons,
      and added the necessary improvements to make it stable, polished and worth releasing.
    </par>
    <par>
      Users of the last official stable release (1.4.3) will notice the quite some new interesting features.
      Most parts of the user interface have been improved. Combined with rich colorful chat styles
      it's a refreshing breeze for the eyes.
    </par>
    <par>
      Enjoy!
    </par>
    <par><link href="/changes/1.5/">see all changes...</link></par>
  </section>

  <section>
    <title>KMess 1.5-pre2 released</title>
    <par>
      18 March --
      With so many fixes and improvements in CVS, it's time to release a second preview
      of KMess 1.5. This release fixes some critical issues of 1.5-pre1.
      Not all reported bugs are be fixed, in that case the release would be called "1.5" already.
      However, it's recommended to install this version, as it greatly improves
      the user experience of KMess.
    </par>
    <par>
      Release highlights:
    </par>
    <list>
      <item>added support for now playing information</item>
      <item>added file transfer previews</item>
      <item>fixed critical bugs with the direct file transfer</item>
      <item>fixed clickable links in the chat window</item>
      <item>fixed problems with emoticon styles</item>
      <item>fixed problems with the installation</item>
      <item>updated many translations</item>
    </list>
    <par>
      The now playing feature detects media from Amarok, Kaffeine, Noatun, KsCD and Juk.
      Other players are added later, based on popularity of the player.
      The current KDE media players have a major advantage here,
      as these applications don't require linking to another library.
    </par>
    <par>
      For the file transfer previews, KMess uses the same KDE functionality Konqueror relies on to create previews.
      This means KMess can send previews of all file types supported in Konqueror, including OpenOffice, HTML and video files!
      This is a great way to impress your friends using Windows.
    </par>
    <par>
      Finally, the chat window has sine neat features for all geeks out there.
      Links without www. or http: are still recognized as links.
      So, writing things like slashdot.org, or dot.kde.org work as well!
      As new option, it's also possible to disable the shaking effect of a nudge.
    </par>
    <par>
      Enjoy!
    </par>
    <par><link href="/changes/1.5pre2/">see all changes...</link></par>
  </section>

  <section>
    <title>Developer blogs added</title>
    <par>
      26 November --
      The website aggregates developer blogs as of today.
      With this feature, we can keep you informed about the progress
      in KMess, idea's for a next release. It will be mixed too with stories
      about Linux / Open Source views, personal thoughts or technical stuff.
    </par>
  </section>

  <section>
    <title>Website moved to www.kmess.org</title>
    <par>
      23 November --
      The KMess website has moved from kmess.sourceforge.net to www.kmess.org.
      This should give the website a nice speed boost. This move also allows us
      to add some interesting things to the website (aggregating blog feeds).
    </par>
  </section>

  <section>
    <title>KMess 1.5-pre1 released</title>
    <par>
      9 October --
      KMess 1.5-pre1 is a preview release, to enjoy newly added features.
      Some MSN features are annoying to miss in a conversation with buddies
      using the official client. Therefore this release adds support for receiving
      winks, nudges, custom emoticons, and offline chat messages.
      File transfers are also improved, and chat windows are more visually appealing now.
    </par>
    <par>
      Release highlights:
    </par>
    <list>
      <item>fast file transfers</item>
      <item>receiving custom emoticons</item>
      <item>receiving winks</item>
      <item>receiving offline-im messages</item>
      <item>receiving and sending nudges</item>
      <item>displaying a personal status message</item>
      <item>rich colorful chat styles</item>
      <item>different emoticon themes</item>
      <item>visually appealing improvements to the user interface</item>
    </list>
    <par>
      It's possible there are minor things that could use some improvement,
      but nothing serious that would be a blocker. Instead of waiting much longer,
      this preview release allows you to enjoy the new features *now*.
      More incremental improvements will appear in the final 1.5 release later.
    </par>
    <par>
      Enjoy!
    </par>
    <link href="/changes/1.5pre1/">see all changes...</link>
  </section>

  <section>
    <title>KMess 1.4.3 released</title>
    <par>
      8 April 2006 --
      KMess 1.4.3 contains some essential bug fixes that were only available
      in the development version until now. This allows everyone to enjoy using
      KMess while the development still continues. No major features were added,
      these will appear later in KMess 1.5. Users and packagers are recommended
      to upgrade to this version.
      For more details, see this <link href="/changes/1.4.3/">list of changes</link>.
    </par>
  </section>

  <section>
    <title>KMess 1.4.2 released</title>
    <par>
      8 November 2005 --
      Some users validly reported KMess crashed on their systems.
      This is something we hope to prevent in the future,
      since it's not fun we have to release another revision to fix crashes.
      KMess also had problems with the font selection in the settings dialog,
      contacts without a display picture,
      and new contacts adding you while you're online.
      For more details, see this  <link href="/changes/1.4.2/">list of changes</link>.
      Those bugs are solved in this new release
      and a translation for Slovenian has been added.
    </par>
  </section>

  <section>
    <title>KMess 1.4.1 released</title>
    <par>
      27 September 2005 -- KMess 1.4.1 has been released.
      It's recommended to upgrade, because this release
      fixes two crashes that made KMess difficult to use.
      One crash happened with MSN Messenger 7.5 clients,
      the other one occured when a contact was invited
      while not being present on your contact list.
    </par>
  </section>

  <section>
    <title>KMess 1.4 released</title>
    <par>
      1 September 2005 -- A new version of KMess has been released.
      This is a maintenance release which fixes many bugs from the previous versions.
      This includes long standing bugs and issues of early KMess versions
      that couldn't be fixed during the 1.4-pre1 release.
    </par>
    <par>
      The most important fixes are the issues with file transfers,
      saving chat logs and settings, crashes, and resuming of chat sessions.
      More information can be found in
      the <link href="/changes/1.4/">list of changes</link>
      and the <link href="http://kmess.cvs.sourceforge.net/kmess/kmess/ChangeLog?rev=1.236&amp;view=auto">Changelog</link>
      file in CVS.
    </par>
    <par>
      You can download the latest packages from
      the <link href="/download/">download page</link>.
      This release also includes experimental support
      for the Autopackage installer, which allows us
      to produce distribution independent installers/packages.
    </par>
    <par>
      New features will be included in the next releases, including:
      user interface improvements, support for MSN Games, webcam receiving
      and MSN protocol enhancements.
    </par>
  </section>

  <section>
    <title>KMess 1.4-pre1 released</title>
    <par>
      8 May 2005 -- There is a new release of KMess, the MSN Messenger client for Linux and KDE.
      This release fixes the login problems, and problems moving contacts to different groups.
    </par>
    <par>
      The new features include:
    </par>
    <list>
      <item>MSN picture transfer support</item>
      <item>an improved contact list dialog</item>
      <item>a new set new emoticons</item>
      <item>a new file transfer dialog</item>
      <item>many small bugs/compilation errors are fixed</item>
      <item>moving contacts is fixed</item>
      <item>login problems are fixed</item>
    </list>
    <par>
      More information can be found at
      the <link href="http://kmess.sourceforge.net#boardviewtopic.php?t=467">forum announcement</link>
      and <link href="/changes/1.4pre1/">list of changes</link>.
    </par>
    <par>
      We could also use your help translating KMess, or creating RPM packages.<newline/>
      If you feel you can help, don't hesitate to contact.
    </par>
  </section>


  <section>
    <title>KMess 1.3 released</title>
    <par>
      15 October 2003 --
      KMess 1.3 has now been released, including many new features from the previous 1.2 release.
      Most importantly, this version supports the new MSN protocol,
      and continues to operate after the lockout of the old protocol.
      Also, there is a new popup balloon system and a powerful chat format editor.
    </par>
  </section>


  <section>
    <title>KMess 1.2.1</title>
    <par>
      16 May 2003 --
      Following numerous bug reports on the forums, KMess 1.2.1 has been released,
      which should fix the compilation problems that some people have been having.
    </par>
  </section>


  <section>
    <title>KMess version 1.2 released</title>
    <par>
      4 May 2003 -
      KMess is available now in a new version. It ships new features like:
    </par>
    <list>
      <item>Users can start as invisible rather than online.</item>
      <item>Users can have blank, unsaved passwords but retain user settings.</item>
      <item>Text can be formatted with /italic/, _underlined_, and *bold*.</item>
      <item>Groups can be reordered.</item>
      <item>You can show or hide offline contacts.</item>
      <item>Emoticons are shown in the contact list.</item>
      <item>Contacts can be dragged to move between groups and dragged into a chat to be invited to it.</item>
      <item>A background picture was added to the contact list.</item>
    </list>
    <par>
      Otherwise many bugfixes were committed to CVS since the last release.
    </par>
  </section>

</page>
