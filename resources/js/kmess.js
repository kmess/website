/*
 * KMess JavaScript file.
 */

if( window.jQuery )  // avoid more errors in IE5.5
{

  $(document).ready( function() 
  {
    // Initialize jQuery fancybox.
    $("a[rel=screenshot]").fancybox();

  } );

}
