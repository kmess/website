#!/usr/bin/perl

use strict;

# get list of all folders
open(SH, "find . -type d |") or die "Can't open find output: $!\n";
my @files = sort <SH>;
close(SH);

chomp(@files);

# open pipe to ftp application
open(FTP, "| ftp www.kmess.org") or die "Can't open ftp program: $!\n";
print FTP "progress off\n";
print FTP "cd /htdocs/\n";
print FTP "mkdir $_\n" foreach(@files);

# process the list of files from the input steam
while(my $file = <STDIN>)
{
  chomp($file);
  print "$file\n";
  print FTP "put $file\n";
}

# quit ftp
print FTP "quit\n";
close(FTP) or die "Can't execute ftp: $!\n";

