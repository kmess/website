<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  
  <xsl:template match="planetMainPage">
    <xsl:text disable-output-escaping="yes"><![CDATA[

    <div class="planetMainPage">
    <!--[[begin results=0]]-->
    <p>There are no RSS entries in the database.</p>
    <!--[[end results=0]]-->
    <!--[[begin results=1]]-->
    <!--[[begin rssitems]]-->
      <!--[[begin newday=1]]--><h2><!--[day]--></h2><!--[[end newday=1]]-->
      <!--[[begin newauthor=1]]--><h3><a class="feed" href="<!--[feed_siteurl]-->"><!--[feed_title]--></a></h3><!--[[end newauthor=1]]-->
      <h4><a href="<!--[url]-->" target="_blank"><!--[title]--></a></h4>
      <div class="content">
        <!--[raw_contents]-->
        <p><!--[fulldate]--></small> - <a class="entry" href="<!--[url]-->#comments" target="_blank">view comments</a></p>
      </div>
    <!--[[end rssitems]]-->
    <!--[[end results=1]]-->
    </div>

    ]]></xsl:text>
  </xsl:template>

</xsl:stylesheet>
