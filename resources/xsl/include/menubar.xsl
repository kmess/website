<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  
  <xsl:template name="menubar">
    <xsl:param name="path" />

    <!-- store the file in a variable -->
    <xsl:variable name="menudata" select="document('../../menu.xml')/menu" />

    <!-- plain menu -->
    <xsl:for-each select="$menudata/group">
      <dl class="box">
        <dt><xsl:value-of select="@title"/></dt>
        <dd>
          <ul class="menu">
            <xsl:for-each select="item">
              <xsl:call-template name="menuitem">
                <xsl:with-param name="curpath" select="$path" />
              </xsl:call-template>
            </xsl:for-each>
          </ul>
        </dd>
      </dl>
    </xsl:for-each>
  </xsl:template>


  <xsl:template name="menuitem">
    <xsl:param name="curpath" />

    <!--
      - $curpath is the full directory path of the current file,
         without a starting slash or ending slash.
      - $path is the path of the current menu item.
    -->
    
    <!-- get $without_startslash from @path -->
    <xsl:variable name="path_without_startslash">
      <xsl:choose>
        <!-- get rid of the starting / -->
        <xsl:when test="starts-with( @path, '/' )">
          <xsl:value-of select="substring-after( @path, '/' )" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="@path" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <!-- get $without_slashes from $without_startslash -->
    <xsl:variable name="path_without_slashes">
      <xsl:choose>
        <!-- get rid of the elements after / -->
        <!--<xsl:when test="ends-with( $path_without_startslash, '/' )">-->
        <xsl:when test="substring( $path_without_startslash, string-length( $path_without_startslash ), 1 ) = '/' ">
          <xsl:value-of select="substring( $path_without_startslash, 1, string-length( $path_without_startslash ) - 1 )"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$path_without_startslash"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <!-- output an li. Include class=onpath for active items -->
    <xsl:element name="li">
      <xsl:if test="starts-with( $curpath, $path_without_slashes )">
        <xsl:attribute name="class">onpath</xsl:attribute>
      </xsl:if>
      <a href="{@path}"><xsl:value-of select="@title"/></a>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>
