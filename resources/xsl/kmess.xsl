<?xml version='1.0'?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <!--
    In case you're wondering what you're looking at:

      This is the transformation sheet that converts
      the KMess XML page sources to the online XHTML output.

    This is done by running "make" in the htdocs root folder.
    All pages will be parsed by "xsltproc" and the result
    is visible online. The final markup is applied with CSS.
  -->


  <!-- imports -->
  <xsl:import href="include/menubar.xsl"/>
  <xsl:import href="include/phpplanet.xsl"/>
  <xsl:import href="include/replace.xsl"/>



  <!-- configure output -->
  <xsl:output method="xml" omit-xml-declaration="yes"
              indent="yes" encoding="utf-8"
              doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
              doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN" />
  <xsl:preserve-space elements="pre"/>



  <!-- External script parameters -->
  <xsl:param name="date" />
  <xsl:param name="path" />



  <!-- Internal variables -->
  <xsl:variable name="kmess_versions"         select="document('../version.xml')/versions/kmess" />
  <xsl:variable name="kmess_version"          select="$kmess_versions/@current" />
  <xsl:variable name="kmess_tarversion"       select="$kmess_versions/@tarcurrent" />
  <xsl:variable name="kmess_version_devel"    select="$kmess_versions/@devel" />
  <xsl:variable name="kmess_tarversion_devel" select="$kmess_versions/@tardevel" />
  <xsl:variable name="needs_fancybox" select="count( //screenshot/@largesrc ) > 0" />



  <!-- Page structure -->

  <!--
    All pages use the <page> root tag.
    It defines the common root structure, menu and sidebars.
    The site logo is displayed as background of the <h1> tag.
    The root structure should be written as:
     <page>
       <meta>
         <title>Page title</title>
       </meta>

       <section>
         ..
       </section>
       <section>
         ..
       </section>
     </page>
  -->
  <xsl:template match="/page">
   <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta http-equiv="Content-Style-type" content="text/css"/>
        <meta http-equiv="Content-Script-type" content="text/javascript"/>
        <meta http-equiv="Content-Language" content="en"/>
        <meta http-equiv="imagetoolbar" content="no"/> <!-- hide IE image toolbars -->
        <title>KMess, MSN / Live Messenger for Linux - <xsl:value-of select="meta/title"/></title>
<xsl:if test="$needs_fancybox"><script type="text/javascript" src="/resources/js/jquery.js"></script>
        <script type="text/javascript" src="/resources/js/jquery.fancybox.js"></script>
        <script type="text/javascript" src="/resources/js/kmess.js"></script>
        <link rel="stylesheet" type="text/css" href="/resources/css/fancybox.css" /></xsl:if>
        <link rel="stylesheet" type="text/css" href="/resources/css/kmess.css" />
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
        <link rel="start" href="/" />
<xsl:if test=" *//planetMainPage ">
        <link rel="alternate" type="application/rss+xml" title="Aggregation of KMess developer blogs" href="/blogs/?rss" />
</xsl:if>
        <meta name="generator" content="KMess XSLT template, {$date}, libxml xslt processor"/>
        <meta name="keywords" content="KMess, MSN, MSN Messenger, Live, Live Messenger, Linux, KDE, Instant Messenging, Instant Messenger, Chat, client, IM, Open Source, Free Software, SuSE, OpenSUSE, Slackware, Debian, Ubuntu, Fedora" />
        <meta name="description" content="A MSN Messenger / Live Messenger client for Linux, aiming at integration with the KDE desktop environment, focus on Messenger-specific features and an easy-to-use interface." />
<xsl:comment><![CDATA[[if IE]>
    <link rel="StyleSheet" href="/resources/css/kmess-ie6.css" type="text/css"/>
<![endif]]]></xsl:comment>
<xsl:comment>
   This page was generated with the libxml xslt processor.

     Date:         <xsl:value-of select="$date"/>
     Source file:  <xsl:value-of select="$path"/>/index.xml
     Template:     resources/xsl/kmess.xsl

   Contact us if you like to know how this works!
   It works really sweet.

</xsl:comment>
      </head>

      <body>
        <div id="top">
          <h1><a href="/">KMess - MSN / Live Messenger for Linux / KDE</a></h1>
          <a class="hide" href="#toContent">Skip menu</a>
        </div>

        <div id="container">
          <!-- left box -->
          <div id="leftwrapper">
            <xsl:call-template name="menubar">
              <xsl:with-param name="path" select="$path"/>
            </xsl:call-template>
          </div>
          <hr />

          <!-- content -->
          <div id="centerwrapper">
            <h2 style='color:red;font-weight:bold;'>This project was discontinued: the website is provided for historical reasons.</h2>
            <h2><a name="toContent" /><xsl:value-of select="meta/title"/></h2>
            <div id="content">
              <xsl:apply-templates select="section"/>
              <div class="clear"></div>
            </div>
          </div>

          <!-- right box -->
          <hr />
          <div id="rightwrapper">
            <dl class="box">
              <dt>Current Version</dt>
              <dd class="middle">
                The current version of KMess is: <xsl:value-of select="$kmess_version"/>
              </dd>
              <dd>
                Latest development version: <xsl:value-of select="$kmess_version_devel"/>
              </dd>
            </dl>
            <dl class="box">
              <dt>Extras</dt>
              <dd>
                <a href="http://sourceforge.net/projects/kmess/"><img src="http://sourceforge.net/sflogo.php?group_id=51677&amp;type=11" width="120" height="30" alt="Get KMess - MSN Messenger client for KDE at SourceForge.net"/></a>
                <div class="ohlohWidgets">
                  <script type="text/javascript" src="http://www.ohloh.net/p/6313/widgets/project_users.js?style=blue"></script>
                  <script type="text/javascript" src="http://www.ohloh.net/p/6313/widgets/project_thin_badge.js"></script>
                  <!--<a href="http://www.ohloh.net/projects/6313?ref=WidgetProjectThinBadge"><img width="100" height="16" src="http://www.ohloh.net/projects/6313/widgets/project_thin_badge.gif" alt="Ohloh project report for KMess" /></a>-->
                </div>
                <a href="http://www.facebook.com/pages/KMess/64607429248"><img src="/resources/img/facebook.jpg" width="80" height="15" alt="Facebook" /></a>
                <a href="http://www.kde-apps.org/groups/?mode=0&amp;id=218"><img src="/resources/img/kde-apps.png"  width="100" height="20" alt="KDE-Apps.org" /></a>
              </dd>
            </dl>
          </div>

          <div class="clear"></div>
        </div>
      </body>
    </html>
  </xsl:template>



  <!-- Section, title, par -->

  <!--
    The <section> tag is the common container for all content.
    You can add title,par,list,toc,shell tags into it.

     <section>
       <title>title1</title>
       <par>
         Text paragraph..
       </par>

       <title>title2</title>
       <par>
         more text
       </par>
     </section>
  -->
  <xsl:template match="section">
    <div>
      <!-- a special detection for MSIE because it does
           not support the CSS2 :first-child selector -->
      <xsl:attribute name="class">
        <xsl:choose>
          <xsl:when test="count(preceding::section/title) = 0">
            <xsl:value-of select="'first_section section'"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="'section'"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>

      <!-- parse the content -->
      <xsl:apply-templates/>
      <!-- <div class="clear"></div> -->
    </div>
  </xsl:template>


  <!--
    The <title> tag should be used in each <section>
    It can be used multiple times to add new titles
    without ending the section.
  -->
  <xsl:template match="section/title">
    <h3><xsl:apply-templates /></h3><!-- use apply-templates not value-of, so <kmessversion/> is supported-->
  </xsl:template>

  <xsl:template match="section/subtitle">
    <h4><xsl:apply-templates /></h4>
  </xsl:template>

  <xsl:template match="question">
    <h4 class="faq_question"><xsl:apply-templates/></h4>
  </xsl:template>


  <!--
    The <par> tag holds one paragraph of text.
    Because of XHTML restrictions, you cannot
    place block elements in a paragaph.
    Add them to the <section> tag directly.
  -->
  <xsl:template match="section/par">
    <p>
      <xsl:if test="@id"><xsl:attribute name="id"><xsl:value-of select="@id" /></xsl:attribute></xsl:if>
      <xsl:apply-templates/>
    </p>
  </xsl:template>



  <!-- bullet lists -->

  <!--
    Bullet lists can be created with:
    <list>
      <item>item1</item>
      <item>item2</item>
      <item>itemN</item>
    </list>
  -->
  <xsl:template match="list">
    <ul class="list">
      <xsl:apply-templates select="item"/>
    </ul>
  </xsl:template>

  <xsl:template match="list/item">
    <li class="bullet"><xsl:apply-templates/></li>
  </xsl:template>



  <!-- links -->

  <!--
    The link tag can be written as:
    1. <link>http://www.google.com/</link>
    2. <link href="http://www.google.com/">Google</link>
  -->
  <xsl:template match="link">
    <xsl:choose>
      <xsl:when test="@href">
        <xsl:variable name="href1">
          <xsl:call-template name="globalReplace">
            <xsl:with-param name="string"      select="@href" />
            <xsl:with-param name="search"      select="'{kmesstarversion}'" />
            <xsl:with-param name="replacement" select="$kmess_tarversion" />
          </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="href">
          <xsl:call-template name="globalReplace">
            <xsl:with-param name="string"      select="$href1" />
            <xsl:with-param name="search"      select="'{kmesstarversion-devel}'" />
            <xsl:with-param name="replacement" select="$kmess_tarversion_devel" />
          </xsl:call-template>
        </xsl:variable>
        <a href="{$href}">
          <xsl:if test="@class"><xsl:attribute name="class"><xsl:value-of select="@class" /></xsl:attribute></xsl:if>
          <xsl:apply-templates/>
        </a>
      </xsl:when>
      <xsl:otherwise>
        <a href="{text()}">
          <xsl:if test="@class"><xsl:attribute name="class"><xsl:value-of select="@class" /></xsl:attribute></xsl:if>
          <xsl:apply-templates/>
        </a>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>



  <!-- images -->


  <!--
    A simple image tag
  -->
  <xsl:template match="image">
    <!-- optional float attribute -->
    <!-- image block -->
    <img src="{@src}" width="{@width}" height="{@height}" alt="{@alt}">
      <xsl:if test="@float">
        <xsl:attribute name="style">
          <xsl:value-of select="concat('float:',@float)"/>
        </xsl:attribute>
      </xsl:if>
    </img>
  </xsl:template>

  <!--
    The screenshot tag can be written as:
    1. <screenshot src="test.gif" width="1" height="1" alt="" />
    2. <screenshot src="test.gif" width="1" height="1" alt="">A caption</image>

    You can also specify a "largesrc" attribute, it will
    make the image clickable, and adds a "zoom icon to the caption.

    The "float" attribute will either float the image left or right.
  -->
  <xsl:template match="screenshot">
    <div>
      <!-- optional float attribute -->
      <xsl:attribute name="class">
        <xsl:choose>
          <xsl:when test="@float">
            <xsl:value-of select="concat('image_box_',@float,' image_box')"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="'image_box'"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <!-- image block -->
      <p class="image_par">
        <xsl:choose>
          <xsl:when test="@largesrc">
            <a href="{@largesrc}" rel="screenshot" title="{text()}"><img src="{@src}" width="{@width}" height="{@height}" alt="{@alt}"/></a>
          </xsl:when>
          <xsl:otherwise>
            <img src="{@src}" width="{@width}" height="{@height}" alt="{@alt}"/>
          </xsl:otherwise>
        </xsl:choose>
      </p>
      <!-- add caption below -->
      <xsl:if test="text()">
        <p class="image_caption">
          <xsl:apply-templates/>
          <!-- Add zoom icon -->
          <xsl:if test="@largesrc">
            <a class="zoomlink" href="{@largesrc}"><img src="/resources/img/zoom.gif" width="35" height="8" alt="(zoom)" class="zoomicon"/></a>
          </xsl:if>
        </p>
      </xsl:if>
    </div>
  </xsl:template>



  <!-- table of contents -->

  <!--
    The <toc> tag is suitable to display a list of
    titles with descriptions (table-of-contents)
    It's written as:

    <toc>
      <item>
        <title>The title</title>
        <url>The url, is optional</title>
        <icon width="x" height="y">An icon, this is optional</icon>
        <description>
          A description of the item.
          Multiple description tags are supported
        </description>
      </item>
      <item>
        ..
      </item>
    </toc>
  -->
  <xsl:template match="section/toc">
    <dl>
      <xsl:attribute name="class">
        <xsl:text>toc</xsl:text>
        <xsl:if test="@class"><xsl:text> </xsl:text><xsl:value-of select="@class" /></xsl:if>
      </xsl:attribute>
      <xsl:apply-templates select="item"/>
    </dl>
  </xsl:template>

  <xsl:template match="toc/item">
    <dt>
      <!-- add the icon if there is one -->
      <xsl:if test="icon">
        <xsl:choose>
          <xsl:when test="url">
            <a>
              <xsl:attribute name="href">
                <xsl:apply-templates select="url"/>
              </xsl:attribute>
              <img class="toc_icon" src="{icon}" width="{icon/@width}" height="{icon/@height}" alt="" />
            </a>
          </xsl:when>
          <xsl:otherwise>
            <img class="toc_icon" src="{icon}" width="{icon/@width}" height="{icon/@height}" alt="" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>

      <!-- add the normal title -->
      <span class="toc_title">
        <xsl:choose>
          <xsl:when test="url">
            <a>
              <xsl:attribute name="href">
                <xsl:apply-templates select="url"/>
              </xsl:attribute>
              <xsl:apply-templates select="title"/>
            </a>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="title"/>
          </xsl:otherwise>
        </xsl:choose>
      </span>
    </dt>
    <!-- TODO: short tag, but this is optional -->
    <xsl:apply-templates select="description"/>
  </xsl:template>

  <xsl:template match="item/description">
    <dd>
      <!-- find out what the class should be -->
      <xsl:variable name="class">
        <xsl:if test="../icon">with_icon</xsl:if>
        <xsl:if test="count(following-sibling::item/description) = 0"> last_description</xsl:if>
      </xsl:variable>

      <!-- add class if it's filled in -->
      <xsl:if test="normalize-space($class) != ''">
        <xsl:attribute name="class">
          <xsl:value-of select="normalize-space($class)"/>
        </xsl:attribute>
      </xsl:if>

      <!-- add contents -->
      <xsl:apply-templates/>
    </dd>
  </xsl:template>



  <!-- shell commands -->

  <!--
    The <shell> tag should be used to display
    commands in a monospaced font. Place it directly
    in the <section> tag (due XHTML restrictions).
    Example:
    <shell
>command1
command2</shell>
  -->
  <xsl:template match="section/shell">
    <pre class="shell"><xsl:apply-templates/></pre>
  </xsl:template>

  <xsl:template match="list/item/shell">
    <pre class="shell"><xsl:apply-templates/></pre>
  </xsl:template>


  <!-- small font styles -->

  <!--
    The <command>, <argument>, <variable>, <file>, <code> tags can
    be used to decorate the text in a <par>..</par> nicely.
  -->
  <xsl:template match="command">  <span class="command"><xsl:apply-templates/></span>  </xsl:template>
  <xsl:template match="argument"> <span class="argument"><xsl:apply-templates/></span> </xsl:template>
  <xsl:template match="variable"> <span class="variable"><xsl:apply-templates/></span> </xsl:template>
  <xsl:template match="comment">  <span class="comment"><xsl:apply-templates/></span> </xsl:template>
  <xsl:template match="file">     <span class="file"><xsl:apply-templates/></span>     </xsl:template>
  <xsl:template match="code">     <span class="code"><xsl:apply-templates/></span>     </xsl:template>


  <!-- other blocks -->

  <!--
    A highlight message.
  -->
  <xsl:template match="important">
    <div class="important">
      <h3><xsl:value-of select="@title" /></h3>
      <xsl:apply-templates />
    </div>
  </xsl:template>


  <!-- Misc tags -->

  <!--
    A kmess version tag, keeps the download page links up-to-date.
  -->
  <xsl:template match="kmessversion">
    <xsl:value-of select="$kmess_version" />
  </xsl:template>

  <xsl:template match="kmesstarversion">
    <xsl:value-of select="$kmess_tarversion" />
  </xsl:template>

  <xsl:template match="kmessversion-devel">
    <xsl:value-of select="$kmess_version_devel" />
  </xsl:template>

  <xsl:template match="kmesstarversion-devel">
    <xsl:value-of select="$kmess_tarversion_devel" />
  </xsl:template>



  <!-- Page formatting hacks -->

  <!--
    The <newline/> tag should be avoided when possible,
    but it's available if you really need it.
  -->
  <xsl:template match="newline"><br/></xsl:template>


  <!--
    The clear tag to put the next elements below all floats.
  -->
  <xsl:template match="clearfloats"><div style="clear:both"></div></xsl:template>



  <!-- raw html -->

  <!--
    Cross-linking between sections
  -->
  <xsl:template match="anchor">
    <a name="{@name}"/>
  </xsl:template>

  <!--
    The <html> tag is an ugly solution to add raw HTML code.
    Use <html><b>test</b></html> for example.
  -->
  <xsl:template match="html">
    <xsl:copy-of select="*|comment()|text()" /><!-- primitive solution, but it works. -->
  </xsl:template>

  <!--
    The <span> tag should be easy to use.
  -->
  <xsl:template match="span">
    <span class="@class"><xsl:apply-templates /></span>
  </xsl:template>

</xsl:stylesheet>

