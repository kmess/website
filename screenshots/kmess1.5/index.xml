<?xml version='1.0'?>

<page>
  <meta>
    <title>1.5 Screenshots</title>
  </meta>

  <section>
    <par>
      These screenshots give a short introduction to the features of KMess 1.5.
    </par>
  </section>

  <section>
    <title>The main dialog</title>
    <par>
      From the first startup screen you can login directly to MSN, no hassle with creating a profile first.
      Just connect and start chatting.
      KMess creates the profile for you automatically.
      The only thing you need to do is enable the "Remember this profile" checkbox.
      When a friend is visiting, you can leave that option unchecked.
      This allows them to connect temporarily to MSN without leaving settings at your computer.
    </par>

    <screenshot float="left" src="loginwindow-small.png" width="171" height="250"
                largesrc="loginwindow.png">The main window of KMess</screenshot>

    <screenshot float="left" src="contactlist-small.png" width="171" height="250"
                largesrc="contactlist.png">The contact list window</screenshot>

    <clearfloats/>
    <newline/>


    <title>The chat window</title>
    <par>
      When you have multiple chat windows open, it's nice to see who you're talking with.
      KMess displays the contacts information clearly visible on the screen, in the right sidebar.
      The list of participants is scrollable with your mouse wheel, so when chatting with many people
      you can easily find the contact you need.<newline />
      The sidebar also has room for more functions, like selecting emoticons.
      There are two tabs to add emoticons: the first is there to let you add the normal MSN emoticons, and the second,
      "My Emoticons", contains your account's custom emoticons, which you can use and exchange with your friends.
    </par>

    <screenshot float="left" src="chatwindow-small.png" width="250" height="193"
                largesrc="chatwindow.png">The chat window with it's default appearance</screenshot>

    <screenshot float="left" src="chatwindow-emoticons-small.png" width="250" height="223"
                largesrc="chatwindow-emoticons.png">The emoticons sidebar of the chat window</screenshot>

    <screenshot float="left" src="chatwindow-custom-emoticons-small.png" width="250" height="223"
                largesrc="chatwindow-custom-emoticons.png">The custom emoticons sidebar of the chat window</screenshot>

    <clearfloats/>
    <newline/>


    <title>Chat styles</title>
    <par>
      Chats can be displayed in all kinds of styles.
      Choose between minimal, slick or visually appealing chat styles.
      When a new chat style is selected, the theme is applied to all existing chats as well.
      Optionally, messages can be grouped. This means that KMess doesn't repeat the contact name
      each time a new line is written.
    </par>

    <screenshot float="left" src="chatwindow-styles-small.png" width="250" height="223"
                largesrc="chatwindow-styles.png">The 'Pure' chat style.</screenshot>

    <screenshot float="left" src="chatwindow-custom-emoticons-small.png" width="250" height="223"
                largesrc="chatwindow-custom-emoticons.png">The 'Fresh' chat style.</screenshot>

    <clearfloats/>
    <newline/>


    <title>Compact chat windows</title>
    <par>
      Does the sidebar consume too much space? Is the chat theme cluttering?
      You can choose to be minimal, use an efficient chat style and hide the sidebar.
      When you want to add some emoticons, the sidebar will temporarily pop up, and
      disappear again after the message is sent.
    </par>

    <screenshot float="left" src="chatwindow-compact-small.png" width="243" height="250"
                largesrc="chatwindow-compact.png">The chat window without sidebar and with 'Classic' chat style.</screenshot>

    <clearfloats/>
    <newline/>


    <title>Winks, nudges and custom emoticons</title>
    <par>
      We want to be sure that you can receive what your friends send to you.
      Therefore, we've added support to receive winks, nudges, custom emoticons and offline chat messages! You can also send nudges and custom emoticons, too.
      When something is not supported, KMess will inform you about this as well.
      So you might not be able to play 'Bejeweled' yet, but at least you can inform your friends about it.
    </par>

    <screenshot float="left" src="chatwindow-winks-nudges-small.png" width="250" height="240"
                largesrc="chatwindow-winks-nudges.png">Winks and nudges in chat messages</screenshot>

    <clearfloats/>
    <newline/>


    <title>Fast file transfers</title>
    <par>
      Files are sent over direct connections whenever possible.
      A small dialog keeps you informed about the progress of all active transfers and allows you to abort any one
      of them at any time.
      In some cases, it might not be possible to create a direct connection.
      KMess will automatically revert to an - albeit slow but stable - indirect connection to transfer the file.
    </par>

    <screenshot float="left" src="transferwindow-small.png" width="250" height="178"
                largesrc="transferwindow.png" >The file transfers progress dialog</screenshot>

    <clearfloats/>
    <newline/>


    <title>Account settings</title>
    <par>
      The settings of KMess can be adjusted for every account.
      This includes nifty settings to start with an MSN status of your choice, show a timestamp in chat messages,
      or forcefully display contact messages in the font you choose.
    </par>

    <screenshot float="left" src="account-settings-small.png" width="245" height="250"
                largesrc="account-settings.png">The account settings dialog</screenshot>

    <clearfloats/>
    <newline/>


    <title>Emoticon themes</title>
    <par>
      The emoticon themes can be changed easily, too.
      KMess provides a full set of MSN emoticons, but is also able to use the emoticon themes already installed in KDE.
      Not every theme supports all MSN emoticons, but KMess will match the emoticon codes to MSN's ones as best as possible.
      At the Custom Emoticons tab, you'll be able to organize your account's custom emoticons.
    </par>

    <screenshot float="left" src="emoticon-settings-small.png" width="245" height="250"
                largesrc="emoticon-settings.png">The 'Emoticons' tab of the settings dialog.</screenshot>

    <screenshot float="left" src="custom-emoticon-settings-small.png" width="245" height="250"
                largesrc="custom-emoticon-settings.png">The 'Custom Emoticons' tab of the settings dialog.</screenshot>

    <clearfloats/>
    <newline/>


    <title>Chat settings</title>
    <par>
      Chat styles are easy to change, just choose the style from the select box.
      Optionally, features like emotions, timestamps and message grouping can be disabled.
      KMess also won't limit you to 16 different colors for your chat messages, just choose whatever nice color you'd like.
    </par>
    <screenshot float="left" src="chatting-settings-small.png" width="244" height="250"
                largesrc="chatting-settings.png">The 'Chatting' tab of the settings dialog.</screenshot>

    <clearfloats/>
    <newline/>


    <title>Integration with KDE</title>
    <par>
      Last but not least, KMess is completely integrated with KDE. It uses your favorite browser to open links to web sites; your email client to send email, and uses your KDE font settings, colors and appearance.
      It also uses KDE for popup balloons, sounds and window effects.
      The sound notification preferences can be changed from KMess, or the KDE control center as well.
    </par>

    <screenshot float="left" src="kde-integration1-small.png" width="250" height="111"
                largesrc="kde-integration1.png">KDE popup balloons.</screenshot>

    <screenshot float="left" src="kde-integration2-small.png" width="250" height="182"
                largesrc="kde-integration2.png">Integration with KDE system notifications.</screenshot>

    <clearfloats/>
    <newline/>


    <title>But wait, there is more...!</title>
    <list>
      <item>Being able to set aliases for your contacts, to get rid of those pesky long names forever.</item>
      <item>Setting your personal status message, and viewing others.</item>
      <item>Receiving Offline-IM messages contacts sent when you were offline.</item>
      <item>Log your chats, and see your chat logs exactly like they were displayed in the chat window.</item>
      <item>Right-to-left language support with mirroring in chat styles.</item>
      <item>Running custom commands for events like "new e-mail".</item>
      <item>Setting an automatic reply message when you're not away.</item>
      <item>Letting your contacts know what you're listening with you favorite music player, and view what they're listening to.</item>
    </list>
    <par>
      Enjoy!
    </par>
    <newline/>
    <par>
      Suggestions or improvements?<newline/>
      Leave us a message at the <link href="/board/">KMess help forum</link> !
    </par>
  </section>

  <section>
    <par>
      View older screenshots from past KMess versions: <link href="../kmess1.4/">1.4</link> - <link href="../kmess1.5pre1/">1.5pre1</link>
    </par>
  </section>
</page>
