#!/bin/sh
# KMess-git script #
# DO NOT CHANGE THE ABOVE TWO LINES!
#
# This script allows users who don't have a clue about git, and don't
# need/want to, to keep following KMess development.
#
#
# authors:
# valerio pilo <vpilo@coldshock.net>
#
# changelog:
# v1..20 initial development
# v21 fixed path in success messages, when cloning
# v22 changed update url to kmess
# v23 added 'status' command
# v24 added -o option and autoupdating everytime
# v25 added config settings at the top of the file
#
# license:
# Do What The Fuck You Want To Public License - http://sam.zoy.org/wtfpl/
#


# user-not-so-configurable options

REPOSITORY_URL="https://github.com/vpilo/kmess.git"
STABLE_BRANCH="kmess-2.0.x"

SCRIPT_URL="http://kmess.coldshock.net/git/script/kmess-git.sh"


##################################################################################################
# Do not edit past this point. Here be dragons

VERSION=26
MYSELF=$(basename $0)
WHEREAMI=$(dirname $0)
TEMPFILE=/tmp/.kmess-git.temp.txt

selfupdate()
{
	rm -f $TEMPFILE

	echo -n "Retrieving latest version... "

	if [ ! -w $0 ]; then
		echo "I'm not writable :("
		return 1
	fi

	wget -t 1 -T 5 -q -O $TEMPFILE $SCRIPT_URL

	if [ "$?" -ne 0 -o "$(head $TEMPFILE -n 2)" != "#!/bin/sh
# KMess-git script #" ]; then
		rm -f $TEMPFILE
		echo "Can't get to the new version :("
		return 1
	fi

	LATEST_VERSION=$(cat $TEMPFILE | egrep "^VERSION=" | sed -re 's/VERSION=([0-9]+).*/\1/' )

	if expr "$LATEST_VERSION" "<=" "$VERSION" > /dev/null; then
		rm -f $TEMPFILE
		echo "Already updated!"
		return 2
	fi

	echo -n "Updating to version $LATEST_VERSION... "

    cd $WHEREAMI
    chmod a+x $TEMPFILE
    mv $TEMPFILE $MYSELF
    cd - > /dev/null

    echo "Done!"
    return 0
}

abortunless()
{
	if [ $1 ]; then
		return 0
	else
		echo "Operation has failed :("
		$2
		exit 1
	fi
}


##################################################################################################

echo "* KMess-Git script, v$VERSION *"

# Check whether we can autoupdate or not
if [ "$1" = "-o" ]; then
	shift
	echo "Offline mode. Not auto-updating."
else
	echo "Auto-updating script from kmess.org - to avoid this, run \"$MYSELF -o $@\"."
	if selfupdate; then
		echo "Starting up new version, hold on..."
		exec $0 $@
		exit 2
	fi
fi


echo


case "$1" in

	clone)
		case "$2" in
			unstable)
				# Clone master
				echo "Cloning KMess, unstable development version (Git master)..."
				git clone $REPOSITORY_URL kmess-unstable
				abortunless "-d kmess-unstable"
				cd kmess-unstable
				git submodule update --init contrib/isf-qt
				abortunless "$? -eq 0" "cd .."
				cd ..

				echo
				echo "KMess unstable cloned: you can find it in $WHEREAMI/kmess-unstable"
				;;

			stable)
				# Clone 2.0.x
				echo "Cloning KMess, stable development version (Git branch 2.0.x)..."
				git clone $REPOSITORY_URL kmess-stable
				abortunless "-d kmess-stable"
				cd kmess-stable
				git submodule update --init contrib/isf-qt
				abortunless "$? -eq 0" "cd .."
				git checkout -b $STABLE_BRANCH origin/$STABLE_BRANCH
				abortunless "$? -eq 0" "cd .."
				git branch -D master # We won't need it here..
				abortunless "$? -eq 0" "cd .."
				cd ..

				echo
				echo "KMess stable cloned: you can find it in $WHEREAMI/kmess-stable"
				;;

			*)
				# Invalid version name
				echo "Invalid version. Valid cloneable versions are 'stable' and 'unstable'."
				exit 1
				;;
		esac
		;;


	update)
		DONE=0

		if [ "$2" != "" ]; then
			echo "Warning: the update command doesn't take parameters: it just updates all cloned versions automatically."
		fi

		if [ -d 'kmess-unstable' ]; then
			DONE=1
			# Update master
			echo "Updating KMess, unstable development version (Git master)..."
			cd kmess-unstable
			git pull origin master
			abortunless "$? -eq 0" "cd .."
			git submodule update
			abortunless "$? -eq 0" "cd .."
			cd ..
		fi

		if [ -d 'kmess-stable' ]; then
			DONE=1
			# Update 2.0.x
			echo "Updating KMess, stable development version (Git branch 2.0.x)..."
			cd kmess-stable
			git pull origin $STABLE_BRANCH
			abortunless "$? -eq 0" "cd .."
			git submodule update
			abortunless "$? -eq 0" "cd .."
			cd ..
		fi

		if [ "$DONE" -eq 0 ]; then
			echo "Did you clone at least one version of KMess with this script?"
			exit 1
		fi
		;;


	status)
		DONE=0

		if [ "$2" != "" ]; then
			echo "Warning: the status command doesn't take parameters: it just checks all cloned versions automatically."
		fi

		if [ -d 'kmess-unstable' ]; then
			DONE=1
			cd kmess-unstable
			LASTCOMMIT=$( git log --oneline -1 )
			abortunless "$? -eq 0" "cd .."
			cd ..
			echo "KMess unstable (Git master) is at: $LASTCOMMIT"
		fi

		if [ -d 'kmess-stable' ]; then
			DONE=1
			cd kmess-stable
			LASTCOMMIT=$( git log --oneline -1 )
			abortunless "$? -eq 0" "cd .."
			cd ..
			echo "KMess stable (Git branch 2.0.x) is at: $LASTCOMMIT"
		fi

		if [ "$DONE" -eq 0 ]; then
			echo "Did you clone at least one version of KMess with this script?"
			exit 1
		fi
		;;


	autoupdate)
		selfupdate
		;;


	*)
		echo "This script was made to allow people who don't know Git to follow the KMess
development process. It allows to perform only the most common operations on
the Git source tree: getting it, and updating it.

Here's an overview of the supported commands:

* To download a copy of the KMess code:
	$ $MYSELF clone stable
	# This gets you the stable development version in a directory named 'kmess-stable'

	$ $MYSELF clone unstable
	# This gets you the unstable development version (not recommended but for the most brave testers)
	# within a directory named 'kmess-unstable'

* To keep your copy updated:
	$ $MYSELF update
	# This updates all the copies you cloned with this script.

* To verify which is the current status of your copies:
	$ $MYSELF status

* To prevent this script from updating itself from the kmess.org website:
	$ $MYSELF -o <command>

* To build KMess you can't use this script, but:
	# you need to follow these steps:
	$ cd <kmess-directory>/build
	$ make

	# then to install it:
	$ sudo make install
	# If that doesn't work, try:
	$ su -c 'make install'
"
		exit 1
		;;

esac


echo
echo "Have a nice day :)"

exit 0
